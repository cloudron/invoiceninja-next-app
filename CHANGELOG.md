[0.1.0]
* Initial version

[0.2.0]
* Update to InvoiceNinja 5.1.25

[0.3.0]
* Add REST API tests
* Fix logo and company documents upload

[0.4.0]
* Update InvoiceNinja to 5.1.26

[0.5.0]
* Update InvoiceNinja to 5.1.27

[0.5.1]
* Fixup artisan cronjobs

[0.5.2]
* Update InvoiceNinja to 5.1.30

[0.5.3]
* Update InvoiceNinja to 5.1.31
* Fix snappdf usage

[0.5.4]
* Update InvoiceNinja to 5.1.32

[0.5.5]
* Update InvoiceNinja to 5.1.34

[0.5.6]
* Update screenshots

[0.5.7]
* Update InvoiceNinja to 5.1.37
* Use laravel queues instead of crontab to improve performance
* Fix email sending
* Set minimum memory limit to 512MB

[0.6.0]
* Update InvoiceNinja to 5.1.40

[0.6.1]
* Update InvoiceNinja to 5.1.41

[0.6.2]
* Update InvoiceNinja to 5.1.42

[0.6.3]
* Update InvoiceNinja to 5.1.43

[0.6.4]
* Update InvoiceNinja to 5.1.47

[0.6.5]
* Update InvoiceNinja to 5.1.47

[0.6.6]
* Update InvoiceNinja to 5.1.48

[0.6.7]
* Update InvoiceNinja to 5.1.49

[0.6.8]
* Update InvoiceNinja to 5.1.52

[0.6.9]
* Update InvoiceNinja to 5.1.53

[1.0.0]
* Update Invoice Ninja to 5.1.55
* First stable relase

[1.0.1]
* Update Invoice Ninja to 5.1.56

[1.0.2]
* Update Invoice Ninja to 5.1.57

[1.0.3]
* Update Invoice Ninja to 5.1.58

[1.0.4]
* Update Invoice Ninja to 5.1.61

[1.0.5]
* Run the artisan scheduler via supervisor

[1.0.6]
* Update Invoice Ninja to 5.1.62
* Set lower memory limit to 1G

[1.0.7]
* Update Invoie Ninja to 5.1.63

[1.0.8]
* Update Invoice Ninja to 5.1.65

[1.0.9]
* Update Invoice Ninja to 5.1.66

[1.0.10]
* Update Invoice Ninja to 5.1.67

[1.0.11]
* Update Invoice Ninja to 5.1.70

[1.0.12]
* Update Invoice Ninja to 5.1.71

[1.0.13]
* Update Invoice Ninja to 5.1.72

[1.1.0]
* Update Invoice Ninja to 5.2.1

[1.1.1]
* Update Invoice Ninja to 5.2.5
* Fixes for Stripe Customer Imports
* Fixes for Reminders

[1.1.2]
* Update Invoice Ninja to 5.2.10

[1.1.3]
* Update Invoice Ninja to 5.2.11

[1.1.4]
* Update Invoice Ninja to 5.2.12

[1.1.5]
* Update Invoice Ninja to 5.2.13

[1.1.6]
* Update Invoice Ninja to 5.2.14

[1.1.7]
* Update Invoice Ninja to 5.2.15

[1.1.8]
* Update Invoice Ninja to 5.2.16

[1.1.9]
* Update Invoice Ninja to 5.2.17

[1.1.10]
* Update Invoice Ninja to 5.2.18

[1.1.11]
* Update Invoice Ninja to 5.2.19

[1.2.0]
* Update Invoice Ninja to 5.3.0
* eWay and Paytrace payment drivers.
* optional flutter renderer

[1.2.1]
* Update Invoice Ninja to 5.3.1

[1.2.2]
* Update Invoice Ninja to 5.3.2

[1.2.3]
* Update Invoice Ninja to 5.3.3

[1.2.4]
* Update Invoice Ninja to 5.3.6

[1.2.5]
* Update Invoice Ninja to 5.3.7

[1.2.6]
* Update Invoice Ninja to 5.3.8

[1.2.7]
* Update Invoice Ninja to 5.3.10

[1.2.8]
* Update Invoice Ninja to 5.3.11

[1.2.9]
* Update Invoice Ninja to 5.3.12

[1.2.10]
* Update Invoice Ninja to 5.3.13

[1.2.11]
* Update Invoice Ninja to 5.3.15

[1.2.12]
* Update Invoice Ninja to 5.3.16

[1.2.13]
* Update Invoice Ninja to 5.3.17

[1.2.14]
* Update Invoice Ninja to 5.3.18

[1.2.15]
* Update Invoice Ninja to 5.3.21

[1.2.16]
* Update Invoice Ninja to 5.3.22

[1.2.17]
* Update Invoice Ninja to 5.3.23

[1.2.18]
* Update Invoice Ninja to 5.3.25

[1.3.0]
* Update Invoice Ninja to 5.3.28

[1.3.1]
* Update Invoice Ninja to 5.3.29
* Add BECS to gateways
* Fixes for Company Switcher
* Performance improvements removing unpaid gateway fees
* Do not mark a 0 draft invoice as paid automatically
* Ensure recurring expenses have numeric for tax_amounts
* Exception handling
* minor fixes for paypal express on failure
* Minor fixes for Gateway TYpes
* Fixes for recurring invoices
* Reference customer when paying with Checkout.com
* Fix for accesing response_summary on payment object
* Fixes for refund
* Apply style fixes
* Disable authorization of Square credit card
* Client payment failure emails
* Update validation rules for update task status
* Minor fixes for create company
* Fixes for deleting an unapplied payment
* Fixes for storing bank details
* Allow BECS to show only for clients in Australia
* Fixes for agreement formatting
* Improve spacing on payment form
* Minor fixes for client numbers

[1.3.2]
* Update Invoice Ninja to 5.3.30
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.30)
* Fixes for Stripe Webhook @LarsK1
* Fix for double Update Entity Webhook call
* Update designs
* Update automatic font resolving
* Login page fixes
* Hide client name on small devices
* Save quietly for payment model
* Remove logo-dark variant from admin templates
* Add client to payments webhook

[1.3.3]
* Update Invoice Ninja to 5.3.31
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.31)

[1.3.4]
* Update Invoice Ninja to 5.3.32
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.32)

[1.3.5]
* Update Invoice Ninja to 5.3.33
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.33)

[1.3.6]
* Update Invoice Ninja to 5.3.34
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.34)

[1.3.7]
* Update Invoice Ninja to 5.3.35
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.35)

[1.3.8]
* Update Invoice Ninja to 5.3.38
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.38)

[1.3.9]
* Update Invoice Ninja to 5.3.39
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.39)

[1.3.10]
* Update Invoice Ninja to 5.3.40
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.40)
* Fixes for edge cases around duplicate contacts in portal
* Fixes for company import
* Add Serbian language
* Add Payment Terms filters
* Ensure zero balance invoices are marked as Paid
* Fixes for Check 3DS
* Stripe Zero Decimal CUrrencies
* Tests for zero decimals
* Fixes for UBL creation
* Fixes for import

[1.3.11]
* Update Invoice Ninja to 5.3.41
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.41)
* Apply border to checkbox/select elements
* Apply style changes
* Minor fixes for invoice service
* fixes for token gateways
* Remove viewed property from invoice import transformers
* Try / Catch on delete PDF
* Change label for credits
* Fixes for handling payment drivers that do not exist in v5
* Reorder action on invoices after payment
* Fixes for Company Import
* Slovak language
* Fixes for composer.lock PHP 7.4 support
* Additional domain restrictions
* Further translations for AP
* Show cancellation dialogue
* Tax Rate Filters
* Task Status Filters
* Company gateway Filters
* Update lang files
* Add option in client statements for filtering by invoice status
* Force public access to PDFs
* Align Reminder cron to Reminder console job
* Fixes for Stripe ACH
* Fixes for Partially deleted payments

[1.3.12]
* Update Invoice Ninja to 5.3.43
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.43)
* Minor adjustments for mollie payment driver
* Ensure PDFs are created when sending recurring invoices
* Fixes for endless reminders
* Minor fixes for postmark message query
* Handle negative payments - update client.paid_to_date
* Slack notifications for bounce/spam emails
* Minor Fixes
* Set locale for migrated companies
* Obfuscate tokens
* Update lang for bank transfer
* Tests for apple pay
* Try/catch for apple pay domain
* Stripe Apple Pay
* Enfore payment_terms to '0' if none is set
* Do not allow an invoice to be created for a deleted client
* Fixes for FlySystem exceptions for corrupt paths
* Add Apple Domain Verification to Stripe Gateways
* Minor fixes for Additional Stripe Field
* Fixes for clean design
* Fixes for mollie payment driver
* Fixes for send_email triggered action not generating PDFs in time.
* Force ZAR for PayFast
* Hide expired quotes from client portal
* Fixes for client portal quote tables
* Touch pdf when marked sent
* Handle 100% gateway fees

[1.3.13]
* Update Invoice Ninja to 5.3.44
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.44)

[1.3.14]
* Update Invoice Ninja to 5.3.45
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.45)

[1.3.15]
* Update Invoice Ninja to 5.3.47
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.47)

[1.3.16]
* Update Invoice Ninja to 5.3.48
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.48)

[1.3.17]
* Update Invoice Ninja to 5.3.49
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.49)

[1.3.18]
* Update Invoice Ninja to 5.3.54
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.54)

[1.3.19]
* Package for Invoice Ninja 5.3.54
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.54)
* Fix PDF generation through snappdf

[1.3.20]
* Update Invoice Ninja to 5.3.55
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.55)
* Add ability to purge clients
* Fixes for rounding display on line items
* Minor fixes for delivery notes with illegal chars
* Fixes for livewire assets path
* Purge clients - password protected route
* Working on csv import refactor
* Tests for basetransformer
* Fixes for tests
* CSV Import

[1.3.21]
* Update Invoice Ninja to 5.3.56
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.56)
* Import CSV refactor
* Tests for importing Products from csv
* Payment Transformer for CSV imports
* Fixes for blank currency_id in manually created payments
* Admin Portal - Hosted
* CSV Payment import
* Vendor Import
* Expense import
* Fixes for project imports
* Fixes for zero percent line item taxes
* Fixes for allowing a deleted invoice to be marked as sent
* Process reserved keywords in Recurring Expenses
* Add currency conversion column

[1.3.22]
* Update Invoice Ninja to 5.3.57
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.57)
* Add vendor relation to invoice
* Timezone calcs
* Fixes for save payment methods dialogue
* Fixes for quantity regression
* Do not set exchange rate if already set
* Allow draft quotes to be approved if accessed directly
* Fixes for currency set for paymentsa
* Set password for first invoice access in Client Portal
* Stub methods for harvesting payment profiles from Auth.net
* Minor fixes for get Payment methods Auth.net
* Add meta data to Checkout payments
* Add variable for payments in invoices
* Add shareable links to client portal

[1.3.23]
* Update Invoice Ninja to 5.3.58
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.58)
* Improve credit card auto fill
* Fixes for WePay Credit Card form
* Update webhook spec for Stripe
* Forward user to quote on conversion
* Fixes for client registration

[1.3.24]
* Allow aliases for client portal domain

[1.3.25]
* Update Invoice Ninja to 5.3.59
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.59)
* Fixes for trailing zeros in quantity column
* Show last invoice in previews
* Set defaults for client filter
* Remove customer profile and payment profile if we are not storing token
* Fixes for domain set on password query:
* Auth.net customer profile
* Extending Auth.net implementation
* Add client public notes to available variables
* Fixes for bad sort query filter
* Working on importing Auth.net customers
* Import clients and payment methods via auth.net
* Fixes for auth.net
* Run time form requests
* Fixes for incorrectly implemented guards
* Fixes for show/hide invitation key
* Clean up for due date days recurring invoices
* Admin Portal - Hosted
* Add back in multiple contact selector
* Add Google Analytics to client portal pages if tracking ID is impleme…
* Zoho Imports
* Freshbooks import
* Fresh Books Import Tests
* Invoice2Go tests
* Invoicely import tests
* Refactor for imports

[1.3.26]
* Update Invoice Ninja to 5.3.61
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.61)
* Minor fixes for quote permissions
* Hide archived payment methods where the gateway has been archived / d…
* Fixes for line spacing
* Performance improvements for client portal
* Admin Portal - Hosted
* Performance improvements for Swoole
* Improve .zip functionality
* Refactor for zip files
* Add in checks for account payments
* Improve Stripe customer importation
* Update composer dependencies to Laravel 8.81

[1.3.27]
* Update Invoice Ninja to 5.3.62
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.62)
* Forward to correct location if modules are disabled
* Translations for dropzone
* Fixes for markdown displaying in client portal
* Add fix for vendor contacts in check data script
* Fixes for SEPA token billing
* Fixes for js formatting
* Allow duplicate Taxes to be created
* Minor fixes for calculations
* Add apple domain verification to stripe config
* Insert Apple Domain Verification into Stripe Gateway config
* Updates for payment currency
* Fixes for payments
* Fixes for tests
* Add unsubscribe landing page for emails
* Add unsubscribe links to emails
* Disable gmail email sending on self hosted if importing from hosted p…
* Refactor for backup update console command
* Fixes for composer
* Fixes for CSV import - ensure clients/vendors have a contact
* Improve quantity resolution
* Fixes for languages

[1.3.28]
* Update Invoice Ninja to 5.3.63
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.63)
* Fixes for key login
* Add helper for Invoice payable amount
* Minor cleanup
* Fixes for CSV import
* Improve refunding from subscription upgrades
* Fixes for viewed entities

[1.3.29]
* Update Invoice Ninja to 5.3.64
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.64)
* Set max tries on import
* Fixes for credits
* Fixes for importing company data
* Fixes for deleting payments

[1.3.30]
* Update Invoice Ninja to 5.3.66
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.66)
* Refactor for bulk downloads
* Admin Portal - Hosted
* Fixes for wave imports
* Forward to correct login location - contacts
* Fixes for plain email template
* Fixes for Stripe SEPA
* Fixes for text emails
* Bump for dependencies
* Fixes for model freshness
* Fixes for WePay authorization failure
* Fixes for backup script
* Minor fixes for backup update

[1.3.31]
* Update Invoice Ninja to 5.3.67
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.67)
* Fixes for migration
* Ensure factories for invoice,quotes,credits have at least todays date…
* Fixes for ACSS payments
* Fixes for eWay
* Checks for company user in check data
* Fixes for entity viewed listener - recurring invoices
* Required fields refactor
* Fixes for required fields
* Fixes for hosted stripe payments
* Move auto bill to +6 UTC
* Slight modification for query of company owner
* Sort order or payment methods
* Adjustments for client balance
* Refactor to use increments
* Refactor to use increments
* Transaction events
* Allow file key for uploads
* Add projects to recurring invoices
* Fixes for balances
* Fixes for required client info
* Transaction logs
* Transaction events
* Fixes for tests
* Update for trial pay - annual discounted plans
* Set x-frame-origin
* Set default send_time for all entities to +6 UTC
* Set default auto bill time to 12:20
* Sort companies by code in client registration page
* Start stop tasks via API
* Fixes for validation on vendors
* Approve quote
* Disable paynow button for ACH payments on submit
* Fixes for importing ACH bank accoun tokens
* Fixes for onboarding migratino
* Update TailWind
* Fixes for exporting projects attached to invoices
* Refactor for trial plan workflow
* Show invoice expense documents
* Add clients and invoices into default templates
* Add gross line total to inclusive item sum calcs
* Fixes for project name
* Fixes for invoice->expenses()
* Fixes for failed migration email
* Fixes for client countries

[1.3.32]
* Update Invoice Ninja to 5.3.68
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.68)
* Fixes for customer translations for contact register page
* Disable transaction events in self hosted
* Fixes for auto bill time scheduling
* Fixes for permissions on list response

[1.3.33]
* Update InvoiceNinja to 5.3.70
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.70)
* Fixes for documents
* Do no auto convert quote if converted by admin user
* Minor fixes for quote approvals

[1.3.34]
* Update InvoiceNinja to 5.3.73
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.73)
* Fixes for database check on setup.
* Replace config:clear with optimize during setup process
* Fixes for designs
* Fixes for white label - pdfs

[1.3.35]
* Update InvoiceNinja to 5.3.75
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.75)
* Fixes for queues trying to call SQS
* Fixes for contact registration
* Fixes for custom messages in client portal
* Fixes for gocardless
* Fixes for index title

[1.3.36]
* Update Invoice Ninja to 5.3.76
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.76)

[1.3.37]
* Update Invoice Ninja to 5.3.77
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.77)
* remove ->increment() methods
* Fixes for payment method create
* Fixes for migration validation
* Documentation for listResponse end point
* Improve invoice payment registrations
* Fixes for frequencyforkey
* Improve memory handling for self update

[1.3.38]
* Update Invoice Ninja to 5.3.78
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.78)
* Fixes for custom messages
* Remove domain verification for apple pay
* Improve client paid to date calc
* Remove authorize method for SEPA
* Admin Portal - Hosted
* Fixes for fillable fields for company
* Company Transformer fixes
* Fixes for string to array issues with converting variables in public notes
* Triggered actions for Recurring Expenses
* Fixes for task edge cases
* Improve test coverage for triggered actions
* Add line breaks to messages
* Improve formatting of support messages
* Update company defaults
* Flutter implemented WYSIWYG editor
* Fixes for regression in Handler.php

[1.3.39]
* Update Invoice Ninja to 5.3.79
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.79)
* Protected sub domains
* Add CHF as the symbol as well as code
* Fixes for seeds
* Force integer for client id
* Fixes for api docs
* Fixes for translations corrupting client savings
* Fixes for eWay descriptions
* Admin Portal - Hosted
* Fixes for ledger
* Reduce transaction retries
* Fixes for coercing types for react UI
* Reduce queries for Invoice POST
* Fixes for regression
* Fixes for contact key login
* Localize timezone for payment date
* Entity translations
* Link converted quote to invoice
* Coerce types for authorize payment error
* Handle user not present in completePurchase method of subscriptions
* Fixes for translation of task statuses
* Do not send notifications to archived/deleted users
* Allow draft credits to be used in payments
* Client Reports
* Prevent double gateway fee removal
* Improve invoice number generation when race conditions encountered
* Update client details in Stripe during a transaction

[1.3.40]
* Update Invoice Ninja to 5.3.80
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.80)
* Coerce default_task_rate to float
* Query lazy loading imrpovements
* Disable lazy load blocker
* restore query logging middleware
* coerce string to int
* Enforce character lengths for authorize fields
* Add Partial Due Date variable
* Fixes for company gateway creation
* Webhooks for Projects
* Updates for self updater
* Gateway fees for PayPal
* Restrict reminders to paid accounts on hosted
* Fixes for client tests
* Fixes for types in settings
* Add back livewire configuration
* Sort statements by due date ascending
* Remove unused includes
* Improve error response from eWay gateway
* Clean up logging

[1.3.41]
* Update Invoice Ninja to 5.3.81
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.81)
* Admin Portal - Next
* Add required fields for Paytrace
* Fixes for purge API docs
* Improve error handling within PayTrace
* Minor fixes
* Fixes for reminders
* Fixes for eager loading

[1.3.42]
* Update Invoice Ninja to 5.3.82
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.82)
* Fixes for statement date
* Max password length 1000
* Improve bulk email performance
* Type checking
* Fixes for type checks
* Generate idempotency key on the frontend
* Use idempotency key from frontend in payment request body
* Admin Portal - Next
* GoCardless Logging
* Fixes for regression - incorrect type setting in ClientSettings

[1.3.43]
* Update Invoice Ninja to 5.3.83
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.83)
* Bug fixes for auth.net - client creation.

[1.3.44]
* Update Invoice Ninja to 5.3.84
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.84)
* Translations for react app
* Fix length of auth.net fields
* Fixes for approve with no conversion of quote to invoice from AP
* Release transactions on failures
* Fixes for localizing company deleted email
* Ensure all recurring invoices have a valid state - post migration
* Change custom_value columns from varchar to text (Allows more information to be stored in custom fields)
* Add file system checks to self checker
* New Schema Dump
* Payable filters
* Disable gateway refund options for GoCardless
* Delete bootstrap/cache folder contents as part of the update process

[1.3.45]
* Update Invoice Ninja to 5.3.85
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.85)
* Export CSVs
* Update Copyright text
* Exports
* Expose export csv routes
* CSV Exports
* Quote CSV exports
* Limit client activities
* Recurring Invoice Export CSV
* Payment Export CSV
* Improve WePay guardian
* Product CSV Export
* Quote Items Export

[1.3.46]
* Update Invoice Ninja to 5.3.86
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.86)
* Fixes for converting company gateway ids for client gateway tokens
* Fixes for login controller
* Ensure relation exists prior to showing button
* Add a slight delay the payment observers in case all the data is not …
* Disable markdown by default
* Refactor Ledger Service
* Fixes for login refactor
* Clean up for email sending API
* Minor fixes for gocardless ACH
* Refactor for ledger serice
* Fixes for gmail notifications when credentials expire
* Minor fixes for support messages
* Fixes for check data
* Minor fixes for wave imports
* Minor improvements to CSV exports
* Minor fixes for ledger adjustments
* Fixes for SEPA auto-billing
* Add statement label to statements
* Tests for expenses
* Fixes for Stripe SEPA auto billing
* Fixes for import
* Fixes for client exports
* Fixes for paypal express gateway fees

[1.3.47]
* Update Invoice Ninja to 5.3.88
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.88)

[1.3.48]
* Update Invoice Ninja to 5.3.89
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.89)

[1.3.49]
* Update Invoice Ninja to 5.3.90
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.90)
* Blade files for email content @CirkaN
* Download report mailable @CirkaN
* Job for creating csv report & sending it to the admin @CirkaN
* Minor fixes for auto billing
* Custom value 4 update to text column
* Fixes for paypal fees
* Refactor for Stripe ACH
* Fixes for trial layout
* Add a System maintenance task
* Clean up for login controller
* Improve handling of missing tokens on login
* Fixes for company ledger
* Additional invoice filters

[1.3.50]
* Update Invoice Ninja to 5.3.91
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.91)
* Fixes for required request parameter
* Fixes for validation rules for exports
* Fixes for data array for migration email
* Fixes for reports
* Handle no report key parameter
* Add logging for google analytics
* Drop redundant column
* Fixes for mollie
* Hide @example.com email address

[1.3.51]
* Update Invoice Ninja to 5.3.92
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.92)
* Page numbering with FPDI
* Page numbering trait
* Page numbering for PDFs
* Minor fixes for self updater
* Enable page numbering logic
* Updated dependencies
* Fixes for self hosted setup

[1.3.52]
* Update Invoice Ninja to 5.3.93
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.93)
* Fixes for page numbering.

[1.3.53]
* Update Invoice Ninja to 5.3.94
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.94)
* Purchase Orders
* Adjustments for Stripe Webhooks
* Configure webhooks on gateway creation
* Auth.net Level 2 tax data
* Fixes for bug that sends multiple emails per import
* Fixes for setting permissions on chrome binary
* Push alter migration fix for schedulers table
* Fixes for recurring invoice schduler

[1.3.54]
* Update Invoice Ninja to 5.3.95
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.95)
* Revert zip package
* Minor fixes for exports
* Fixes for email images in outlook
* Improve quality of email formatting for outlook

[1.3.55]
* Update Invoice Ninja to 5.3.96
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.96)
* Fixes for email previews
* Add schema for inventory management.

[1.3.56]
* Update Invoice Ninja to 5.3.98
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.98)
* Purchase orders
* Send email set on blank contact vendor
* Handle negative surcharges
* Adjust memory settings for queue work
* Scaffold for PDF generation for purchase orders
* Vendor PDFs
* Translations for purchase orders
* Add client include for quotes
* Fixes for variable naming
* Purchase Order Actions
* Update for invoice designs
* Fixes for invoice sum calculations
* Fixes for csv imports - exclude deleted data
* Update product request
* Update language files for inventory management
* Add activity events for purchase orders
* Update front end dependencies
* Remove logging for google analytics data
* Purchase Order Events
* Event / Listeners for Purchase Orders

[1.4.0]
* Update Invoice Ninja to 5.3.99
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.99)
* Fix installation on ubuntu 22
* Email display name support
* Clean up logging
* Hide surcharges on PDF if the value is blank
* Ensure client contact id is being return in activity
* Store silent in session to prevent entities being viewed by admins
* Fixes for quote form request

[1.4.1]
* Update Invoice Ninja to 5.3.100
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.3.100)
* Add Vendor guard
* Map vendors api routes
* Vendor route file
* Add Microsoft provider for oauth
* Install Apple driver for socialite
* Minor fixes for client portal badges

[1.5.0]
* Update Invoice Ninja to 5.4.3
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.4.0)
* Minor fixes for correct disk configuration when using S3
* Stubs for vendor portal
* Fixes for badge color shading
* Remove old snappdf installations prior to upgrading
* Accept a purchase order
* Accept signatures on purchase orders
* Update composer dependencies
* Vendor profile update
* Checkout v2 refactor
* Minor fixes for recurring invoices
* Add switch to force react as front end

[1.5.1]
* Update Invoice Ninja to 5.4.4
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.4.4)
* Fixes for reminders when no due date is set
* Add dedicated :MONTHYEAR reserved keyword
* Ensure we save auto_bill_tries
* Updates for hosted platform
* Minor fixes for bcc's
* Add logging for payment intents

[1.5.2]
* Update Invoice Ninja to 5.4.6
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.4.6)
* Forte gateway added. Thanks @kishanmnpatel
* Add microsoft oauth to index.html
* Compiled js file added to public folder.
* Fixes for purchase order fillable list
* Fixes for designs
* Checks for access token property
* Improve queries for client statements
* Appropriately refresh sending tokens
* Admin notifications
* Spam notifications
* Change purchase order transformer type
* Add signup checks to flutter routes
* Fixes for stripe webhooks
* Minor fixes for Purchase orders
* Reduce scopes for microsoft email
* Password protection route with Microsoft OAuth
* Always ensure contacts can pay an invoice with an invitation link
* Bulk actions for purchase orders
* Minor fixes for connected accounts
* Minor fixes for create user route
* Minor fixes for company imports
* Purchase order decorators
* Add flagging abilities to accounts table
* Fixes for paths
* Swiss QR codes
* Templates for Purchase Orders
* Fixes for duplicate quote approved notifications
* Fixes for type checking for purchase orders
* Fixes for surcharge visibility

[1.5.3]
* Update Invoice Ninja to 5.4.7
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.4.7)
* BUGFIX - Show recurring invoice thanks @yois615
* Fixes for tests when calling classes outside app scope
* Hide custom values that are empty
* Hide purchase orders from UI

[1.5.4]
* Update Invoice Ninja to 5.4.8
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.4.8)
* Additional translations
* Minor fixes for mailer
* New Account Notification
* Fixes for template engine
* Purchase Order Emails
* Minor fixes for import
* Fixes for send email option inside invoice

[1.5.5]
* Update Invoice Ninja to 5.4.9
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.4.9)
* Remove paid to date from credit PDF
* Uploads for purchase orders
* Add quantity to subscriptions
* Expense a Purchase Order
* Inventory management from purchase orders
* Update translations
* Introduce Bulgarian translations
* Fixes for migrations
* Fixes for language seeder
* Fixes for purchase order to expense
* Fixes for send_email bulk routes
* Minor fixes for invoice filters

[1.5.6]
* Update Invoice Ninja to 5.4.10
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.4.10)
* Pad out expense when converted from Purchase Order
* Pad out expense when converted from Purchase Order
* Handle pay now redirects offsite
* Verify hash
* Add index to payment hashes
* Fixes for indexes
* Do no alter client balance after an invoice has been marked as paid
* Apple ID AUth
* Allow forced sending of first recurring invoice
* Silence bounce notifications
* Add webhook endpoint when connecting Stripe
* Wrap paid to date in transaction
* Apple OAuth
* Fixes for wrong payment types
* Start recurring invoice on send now
* Add status and client to task transformer
* Company Import|Export for purchase orders
* Add rules for invitations
* Translate entities

[1.5.7]
* Update Invoice Ninja to 5.4.11
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.4.11)
* Update Stripe responses for redirect_status
* Minor fixes for stripe
* Fixes for purging company data
* Fixes for invoice status if balance changes to negative
* Fixes for relations

[1.5.8]
* Update Invoice Ninja to 5.4.12
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.4.12)
* Button for Client Portal @yois615
* Add missing translations for task statuses
* Add exchange rate to factory
* Fixes for purchase orders and business design
* Add support for showing related entity documents on invoice
* Working on signatures
* Ensure signature is provided in client portal prior to moving to next…
* Fixes for quote approve button when signature dialog is closed
* Force delete activities on purge data
* Prep for Hebrew
* Fixes for task statuses
* Add hebrew to repo
* Client include for expenses
* Vendor include for expenses
* Fixes for vendors
* Fixes for mailer
* Notification for changes to v5.5

[1.6.0]
* Update Invoice Ninja to 5.5.2
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.2)
* Minor fixes for date format migration for new installations
* Update to php 8.1

[1.6.1]
* Update Invoice Ninja to 5.5.3
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.3)
* Fixes for subscriptions - allow currency id to be passed through
* Add enabled_expense_tax_rates
* Fixes for BPP in subscriptions
* The "with" filter for the QueryFilters
* Define "with_property" for ProductFilters
* Fixes for tests
* Improve efficiency of queries relying on Country
* Fixes for livewire
* Fixes for Twilio
* Fixes for settings purchase order designs
* Minor fixes for check data
* Fixes for vendor documents
* Vendor documents
* Fixes for emails - remove mime types
* Rebasing commits from v5-develop
* Fixes for multidb + twilio
* Integrate twilio
* Fixes for lock

[1.6.2]
* Update Invoice Ninja to 5.5.4
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.4)
* Fixes for types

[1.6.3]
* Update Invoice Ninja to 5.5.5
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.5)
* Minor fixes for the client portal where the payment methods was not displaying

[1.6.4]
* Update Invoice Ninja to 5.5.6
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.6)
* Fixes for stripe webhook paths
* Fixes for bug in payments.js causing terms to be unacceptable
* Bug fixes for BCC in O365 + Gmail drivers

[1.6.5]
* Update Invoice Ninja to 5.5.7
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.7)
* General improvements and bug fixes.

[1.6.6]
* Update Invoice Ninja to 5.5.8
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.8)

[1.6.7]
* Update Invoice Ninja to 5.5.9
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.9)
* Recurring invoice due on receipt @yois615
* Relocate portalButton variable @yois615
* System logger added for Forte. @kishanmnpatel
* Fixes for phpunit - github actions
* Centralize where we inject email tags
* Remove cs-fixer from github actions

[1.6.8]
* Update Invoice Ninja to 5.5.10
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.10)
* Additional checks for checkout.com
* Add missing fields for activity transformer
* Paytrace cleanup
* Improve subscription currencies

[1.6.9]
* Update Invoice Ninja to 5.5.11
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.11)
* Psalm Cleanup.
* Fixes for ZOHO imports.
* Portuguese translations updated.
* Fixes for Postmark delivery.

[1.6.10]
* Update Invoice Ninja to 5.5.13
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.13)
* Add validation for task time logs
* Fixes for single route actions
* Fixes for base path for canvas kit
* Fixes for showing fees in both the product and tax tables
* Change the order of email filters
* Tests for deleting an invoice
* Fixes for edge case when deleting an invoice with a partial payment
* Clean up for tests
* Remove snappdf download from composer script

[1.6.11]
* Update Invoice Ninja to 5.5.14
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.14)

[1.6.12]
* Update Invoice Ninja to 5.5.15
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.15)
* Update user observer to dispatch a job
* Set relative path for canvaskit
* Clean up for mail listener
* Fixes for task requests
* Adjust credit balance on client record
* Working on client credit balance field
* Tests for credit balance
* Update credit balances when a payment is deleted
* Add daily checks for credit balances
* Fixes for restoring a credit
* Refactor for payments to improve query efficiency
* Refactor payment queries for improved efficiency
* Valid credits rules

[1.6.13]
* Update Invoice Ninja to 5.5.16
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.16)
* Prevent negative credits from being applied as payments to a invoice
* Ensure correct PDF is displayed to the contact
* Fixes for logo width for outlook emails

[1.6.14]
* Update Invoice Ninja to 5.5.17
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.17)
* Add vendor & expense includes for purchase orders
* Add missing translations
* Add new column for invoice_task_project
* Fixes for project with both project and client id
* Wrap client paid to date in transaction
* Toggle sms verification based on domain

[1.6.15]
* Update Invoice Ninja to 5.5.18
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.18)
* Minor fixes for invitation in view portal
* Fixes for showing invoice
* Allow client variables in purchase orders
* Refactor trial confirmed page
* Fixes for applying payment number
* Clean up login controller
* Catch and remove updates for oauth_provider_id
* Add checks for oauth provider id
* Fixes for github actions
* Use transaction when marking an invoice as paid
* Adjust spam filtering
* Minor fixes for templates
* Formatting for client emails
* Check for exec function prior to attemping to use it
* Minor fixes for client filter sorted by display name
* Support for vendor and vendor contact imports
* update vendor csv for tests
* Refactor recurring invoice query
* Improve query efficiency
* Fixes for transforming vendor contacts
* Add indexes to DB

[1.6.16]
* Update Invoice Ninja to 5.5.19
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.19)
* Stripe Imports: Check for valid bank account property before attempting to iterate
* Set required fields to lazy
* Refactor for switch plans
* Better logging around setLateFee()
* Better logging around setLateFee()
* Fresh client prior to updating
* Update project name to just project for label
* Add option to send payment email when invoice has been marked as paid
* Fixes for canvaskit path
* Fixes for inventory notifications

[1.6.17]
* Update Invoice Ninja to 5.5.20
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.20)
* Add check if plan is expired
* Add checks for ninja portal urls
* Improve handling of purchaseOrder and purchase_order
* Updates for vendor routes
* Client Paid To Date updates
* Fixes for tech template
* Fixes for braintree
* Update client presenter methods
* Updated composer lock
* Fixes for default mark down settings
* Fixes for tests.
* Fixes for auto billing when using credits
* Working on credit paymenta
* Fixes for store payment request
* Ensure documents array is an array
* Handle single contact object passed into array
* Handle single contact object passed into array
* Fixes for handling partial payments with credits
* Fixes for stripe error message when authorizing credit card
* Fixes for seeding languages
* Refactor for client balance
* Fixes for select box background color
* Fixes for registration fields and ensure client contact email is uniq…
* Remove redundant code from CompanySettings
* CLean up code paths for client balance
* Remove DispatchNow()

[1.6.18]
* Update Invoice Ninja to 5.5.21
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.21)
* Add secondary font as a variable in HTMLENGINE
* Add additional field for reports to companies table
* Fixes for InputBag
* Fixes for race condition when saving expense numbers
* Improve date resolution in recurring invoices
* Improve float parsing in csv imports
* Additional checks for GoCardless webhooks
* Improve efficiency of lightlogs
* Fixes for vendor templates
* Fixes for sending purchase orders when using a custom template
* Change [] for vendors to contacts.company
* Move lightlogs back to batching

[1.6.19]
* Update Invoice Ninja to 5.5.22
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.22)
* Configuration for in app purchases
* Move lightlogs to batch
* Improve client imports
* Add recurring invoice id to expense when it is generated
* Fixes for support messages
* Fixes for check data
* Minor fixes for support messages
* Ensure we also query trashed client records before lock
* Logging around generates counter
* Additional logging in generates counter
* Minor fixes + additional logging
* Fixes for applying numbers
* Fixes for entity policy not capturing recurring invoice policies
* Update customer details
* Fixes for restricting csv imports
* Add redundancy checks when creating task numbers
* Port entity policy changes to view() policy
* Set SEPA stripe token payments to pending
* Fixes for QR Ibans with no payment references
* Fixes for adding a payment method with Checkout.com
* Adapt Email entity sending using emailentity
* Fixes for sending custom templates with purchase orders
* Add webhook functionality to credits
* Fixes for tests

[1.6.20]
* Update Invoice Ninja to 5.5.23
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.23)
* Fixes for send now functionality of recurring invoice
* Single root element for wepay signups
* Fixes for single root - Livewire
* Update resources to defer
* Minor fixes for client portal queries
* Optimize css bundles
* Fixes for tests
* If blank exchange rate is sent, force 1

[1.6.21]
* Update Invoice Ninja to 5.5.24
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.24)
* Working on hosted upgrades
* Improve trial page
* Increase backoff period
* Force company currency for purchase orders
* Ensure we do not remove gateway fee prematurely
* Fixes for payment layouts (Regression)
* Exclude canvaskit path for hosted
* Implement 3DS for Braintree
* Fixes for seeders
* Fixes for checkout.com customer request class

[1.6.22]
* Update Invoice Ninja to 5.5.25
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.25)
* Minor fixes for stripe imports
* Do not serialize company after migration
* Put in checks for client filters
* Show labels of custom fields in registration form
* Remove direct google URLs
* Minor fixes for QR Code generation for live previews
* Improve livewire performance with Defer
* Fixes for sms verification list
* Minor fixes for Stripe ACH Verifications
* Fixes for wepay

[1.6.23]
* Update Invoice Ninja to 5.5.26
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.26)
* Remove authorize for SEPA
* Minor fixes
* Fixes for generating counters
* Strip tags from client and user nameS
* Remove redundant code in save company request
* fix: SwissQr postal code appears twice thanks @filo87
* Return early to improve PDF generation performance
* Fixes for WePay

[1.6.24]
* Update Invoice Ninja to 5.5.27
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.27)
* Improve Expense Imports
* Improve Stripe ACH when using microdeposits
* Additional DB Queries
* Fixes for non-admin users hitting list views

[1.6.25]
* Update Invoice Ninja to 5.5.28
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.28)
* Add key to account transformer
* Minor updates for htmlengine
* Fixes for forte payment driver
* Fixes for subscription renewals

[1.6.26]
* Update Invoice Ninja to 5.5.30
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.30)

[1.6.27]
* Update Invoice Ninja to 5.5.31
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.31)
* Performance improvements for flutter UI

[1.6.28]
* Update Invoice Ninja to 5.5.32
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.32)
* Fixes for purge client
* Add Preloader
* Fixes for document indexing
* Fixes for indexes
* Add to fillable
* Fixes for storage paths in self host
* Clean up for preload
* Minor fixes for recurring invoices
* Handle invoices going from zero balance to positive balance
* Fixes for refund activity notes
* Minor fixes for filters
* Minor changes for setup intents
* Minor fixes for check data

[1.6.29]
* Update Invoice Ninja to 5.5.33
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.33)
* MatchBankTransactions
* Working on matching transactions
* Expense categories
* Transaction expense resolution
* Resolve categories
* Add provisional match
* Auto pull transactions when linking accounts
* Fill from_date to be one year in the past
* Add transaction id to expenses and payments
* Fixes for bank integration routes
* Allow Bank Accounts to be created manually
* Save Bank Transaction manually
* Bank Transactions matching default category ID
* Always force a baseType
* Return list of bank transactions
* Minor fixes for braintree 3DS
* Disable auto billing when an invoice has been refunded.
* Adjust status if invoice is modified after payment
* Attempt to import documents into new company

[1.6.30]
* Update Invoice Ninja to 5.5.34
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.34)
* Remove sentry from self host
* Fixes for purchase order viewed activity
* Additional checks for exchange rates
* Fixes for Yodlee Test
* Fixes for bank transaction tests
* Set company defaults for expenses when matching bank transactions
* Wipe OAuth data when changing email addresses
* Fixes for tests
* Improve check data
* Minor fixes when calculating invoice cancellations
* 2FA reset
* Fixes for Payment Previews
* Order designs in ascending order
* Restore paymentable reliably when restoring invoice with attached pay…
* Pass references instead of full models into auto bill jobs
* Restrict size and amount of jobs
* Fixes for breaking change in CreateInvoicePdf
* Updates for bug with Sentinel / Redis

[1.6.31]
* Update Invoice Ninja to 5.5.35
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.35)
* Fixes for handling restoring a deleted invoice
* Fixes for settings checks

[1.6.32]
* Update Invoice Ninja to 5.5.36
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.36)
* Fixes for Swiss QR
* Remove inventory adjustments when deleting a invoice
* Remove RFC requirements for email on setup

[1.6.33]
* Update Invoice Ninja to 5.5.37
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.37)
* Merge PDFs
* Fixes for failed payments when the paypal credentials are incorrect
* Fixes for stripe autobilling
* Don't link entities when we are in a transaction
* Fixes for payment email preview
* QR codes for invoices as a variable
* Return reminders to dispatch

[1.6.34]
* Update Invoice Ninja to 5.5.38
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.38)
* Allow individual SMTP per company
* Add verified phone number flag
* Check users number for validity for SMS verifications
* User Twilio API to verify phone numbers
* User Twilio API to verify phone numbers
* Update phpunit spec for github actions
* Update translations
* Fixes for imports
* Datamapper classes
* Return early in some hosted jobs
* Override phone number with international format
* Fixes for bank transaction imports
* Minor fixes for validation
* Minor fixes for Bank Transactions / Integratiosn
* Improve error handling with Checkout Payment Gateway

[1.6.35]
* Update Invoice Ninja to 5.5.39
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.39)
* EPC QR Codes
* Additional logging for Checkout transactions
* Improvements for using react with self host
* Minor fixes for updating a recurring invoice
* Minor fixes for negative invoices
* Change UX for downloading documents from the client portal

[1.6.36]
* Update Invoice Ninja to 5.5.40
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.40)
* Process Invoice Numbers which includes letters
* Flip the request requirements
* Save default terms/footers
* Allow different mail from / mail name for multi company mailers
* Patch for 2FA Verification
* Triggered actions for Credits / Quotes
* Expired quote notifications
* Fixes for uses_inclusive_taxes with recurring invoices
* Fixes for bank transaction imports
* Reset event delay
* New Design
* Update Readme

[1.6.37]
* Update Invoice Ninja to 5.5.41
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.41)
* Add a new design - Calm
* Fix migrations for new invoice design
* Fixes for gocardless events
* Prevent deleted/archived/completed transactions from being re-converted
* Fixes for bank transaction tests
* Fixes for playful design
* Stubs for search authorize.net
* Fixes for gocardless delayed instant bank payment notification:
* Add a Paid CSS Overlay to invoice designs using the variable $status_logo
* Late fee tests
* Tests for recurring invoice variables
* Minor fixes for date range calculation for :WEEK
* Fixes for Fortre payment driver and where no service fees are applicable
* Clean input for custom css
* Implement additional filters for list views

[1.6.38]
* Update Invoice Ninja to 5.5.42
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.42)
* [Feature] Bank Transaction Rules
* [Feature] API Doc Blocks for Bank Transaction Rules
* [Bug fix] Fixes for failed email mailer, edge case where balances may not adjust
* [Bug fix] Adjust when we send certain events after an email failure
* [Feature] Match Bank Transactions
* Tests for matching expenses
* Transaction rules tests
* [Bug fix] Remove delay from events
* [Bug fix] Escape variables in custom designs
* Minor fixes for create company routes
* [Bug fix]Catch EPC QR failures

[1.6.39]
* Update Invoice Ninja to 5.5.43
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.43)
* Spread out cron jobs to even system load
* Updates for SMS confirmations
* Update twilio sms authorize() method
* Improve cron definitions to prevent race conditions
* Improve validation layer for reports
* Minor refactor for reminder jobs
* Refactor the way we execute scheduled commands
* Use translation for request cancellation
* Clean up subscription service
* Fix for subscription cancellation where there are no outstanding invoices
* Fixes for displaying correct quote status in client portal
* Minor fixes for bank transaction imports
* Fixes for creating backup directory if it does not exist
* Minor fixes for type checks
* Minor fixes for failed notifications
* Don't return value from void function
* Refactor reminder crons
* Fixes for client number race conditions when importing stripe clients
* Fixes for stripe importing customers
* Wind back Middleware for Bank Service Matching
* Fixes for updated_at in bank_transaction_rules
* Change private to protected property for middleware key
* Inject small delay into email dispatch
* Implement chunking of auto bill data
* Handle nothing being passed into sort() filter
* Fixes for missing properties in store bank integration request
* Minor fix for client balance adjustment
* Add more entropy when updating company ledger
* Fixes for demo mode
* Refactor for Stripe payment intents + charges + new api

[1.6.40]
* Update Invoice Ninja to 5.5.44
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.44)
* Translate Stripe payment texts by @LarsK1 in #7999
* Bug Fix - Client balance corrupted when transaction is deleted. by @turbo124 in #8010
* #8008 csv import auto detect delimiter by @checkitsedo in #8009
* Refactor PDF attachments as data instead of file paths
* Inject slight delay into emails
* Translate Stripe payment texts thanks @LarsK1
* LarsK1 committed 22 hours ago
* Implement checkout object for phone
* Minor fixes for Stripe Payment Intent query
* Fixes for store bank transaction
* Add frequency to recurring invoice export
* Fixes for storing bank transactions
* Add CSV delimiter Autodetection thanks @checkitsedo
* Fixes for CSV imports, replace work_phone with phone
* Fixes for applying payment from a transaction, and then unwinding the…
* Fixes for multiple deletes on a single invoice
* Do not allow restoration of a invoice with a deleted payment
* Fixes for expense category validation
* Fixes for Stripe payment translations

[1.6.41]
* Update Invoice Ninja to 5.5.45
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.45)
* Add Payment id to bank transactions table
* Tests for linking expenses and payments
* Add css class to enable resizing of payment QR Codes
* Increase chunk size for autobill
* Fixes for payment tests
* Fixes for Zoho Import
* Fixes for single account creation
* Add form request for enable two factor authentication
* Updates for translations for Stripe
* Additional checks for reminder scheduling for endless reminders
* Updates for cookie consent - translate to locale if available
* Ensure due_date_days is populated
* Fixes for edge cases with recurring invoice due date days = 0
* Count Bank Account Created on Hosted

[1.6.42]
* Update Invoice Ninja to 5.5.46
* Use Cloudron base image 4.0.0
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.46)
* Provide translations by @beganovich in https://github.com/invoiceninja/invoiceninja/pull/7972
* Fixes for custom labels/values for credits by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8030
* Add status to client export by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8032
* Reserved keywords are aware of date in recurring invoice PDF preview by @talkstraightuk in https://github.com/invoiceninja/invoiceninja/pull/7979
* Update Sentry Laravel SDK to v3 by @cleptric in https://github.com/invoiceninja/invoiceninja/pull/8026
* Tests for linking payments with expenses by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8033
* Add delete webhook for projects by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8034

[1.6.43]
* Update Invoice Ninja to 5.5.47
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.47)
* Add payment id to bank transaction matching
* Expand document name in client portal from 20 -> 40 chars
* Ensure we retrieve archived invoices in Stripe payment webhook
* Check for properties before accessing
* Improve bank transaction filters
* Ensure client is tagged on view quote activity
* Add convert currency properties to company table
* Subscriptions v2

[1.6.44]
* Update Invoice Ninja to 5.5.48
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.48)
* Add support for Matomo
* Add Matomo to database table
* Fixes for Matomo URL/ID
* Add idempotency key for mollie
* Fixes for Klarna
* Add missing translations
* Fixes for phone validation
* Add checks prior to attempting to add in payment types
* Add transaction filters for payments and expenses
* Minor clean up of layout files
* Use redirect away for mollie
* Disable auto billing on recurring invoices and generated invoices
* Fixes for type hints
* Catch mollie payments if redirect URL is null
* Add proper description to mollie credit card transactions
* Minor fixes for stripe translations
* Subscriptions v2 (WIP)

[1.6.45]
* Update Invoice Ninja to 5.5.49
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.49)
* OTP for subscriptions by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8069
* Quote filters for expired and upcoming by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8071
* Change Klarna intregration to comply with Klarna's rules by @LarsK1 in https://github.com/invoiceninja/invoiceninja/pull/8059
* Force a currency for vendors by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8074
* Subscriptions v2 by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8076
* Attach invoice documents to payment emails by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8077
* Disable auto fill for credit cards by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8078
* minor fixes for subscriptions by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8080
* v5.5.49 by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8081

[1.6.46]
* Update Invoice Ninja to 5.5.50
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.50)
* Flip sort order for payments in client portal by @turbo124 in #8085
* Minor fixes for tax currencies - vendor vs client by @turbo124 in #8089
* Fixes for Matomo by @LarsK1 in #8083
* Fix eWay failures related to 32-bit integer limit by @joshuadwire in #8095

[1.6.47]
* Disable auto update checker

[1.6.48]
* Update Invoice Ninja to 5.5.51
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.51)
* fix SEPA QR Code recurring invoice by @Hobby-Student in #8105
* adding `update_payment` webhook by @paulwer in #8118
* Product Sales Report by @turbo124 in #8119
* Minor improvements for checkout 3ds processing by @turbo124 in #8120

[1.6.49]
* Update Invoice Ninja to 5.5.52
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.52)
* Add issue templates back into the repo by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8126
* Additional mailer implementation by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8129

[1.6.50]
* Update Invoice Ninja to 5.5.54
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.54)
* Fix for leak on products route when using the ?with filter by @turbo124 in #8137

[1.6.51]
* Update Invoice Ninja to 5.5.55
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.55)

[1.6.52]
* Update Invoice Ninja to 5.5.56
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.56)
* Fixes for deleting payments by @turbo124 in #8151
* Minor fixes for transactions by @turbo124 in #8152
* Fixes for client mailers by @turbo124 in #8153
* Quote Filters by @turbo124 in #8156
* Fixes for matomo by @LarsK1 in #8157

[1.6.53]
* Update Invoice Ninja to 5.5.57
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.57)
* Implement next_run calculation for scheduled tasks by @turbo124 in #8172
* Add company logo size to company settings object by @turbo124 in #8173

[1.6.54]
* Update Invoice Ninja to 5.5.60
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.60)
* Fixes for clientcontact observer bug

[1.6.55]
* Update Invoice Ninja to 5.5.61
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.61)
* Fixes for Client Portal Bug
* Fixes for Multidb Task Scheduler
* Setup permissions for Bank Transactions
* Fixes for content-disposition in CORS
* Add filename to headers for inline files

[1.6.56]
* Update Invoice Ninja to 5.5.62
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.62)
* enhance address field by @Hobby-Student in #8188
* fix: processReservedKeywords should be aware of recurring invoice date by @talkstraightuk in #8182
* fix for permissions regression

[1.6.57]
* Update Invoice Ninja to 5.5.63
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.63)
* Ensure api token has a name using update route by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8211

[1.6.58]
* Update Invoice Ninja to 5.5.64
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.64)
* Adjust throttling for password reset requests
* Minor checks for user deleted_at state
* Set timezone when testing date ranges
* Update clean design to allow clean page breaks
* Updates for bug template
* Implement logic that changes the response based on user permissions
* Fixes for validation of webhooks
* Live Design Preview Test
* Skip live designs in github actions
* Enable setting the width of the logo via settings
* Update designs
* Fixes for reports

[1.6.59]
* Update Invoice Ninja to 5.5.65
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.65)
* Fixes for Job Retries in Mailers and Webhooks by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8220
* Minor fixes for create entity when models are missing by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8221
* Minor fixes for the Clean design by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8222
* Minor fixes for the task scheduler by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8223
* Fixes for ninja mailer by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8224
* Fixes for staticmodel scopes by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8228
* v5.5.65 by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8229

[1.6.60]
* Update Invoice Ninja to 5.5.66
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.66)
* Adding additional filter methods by @paulwer in #8227
* Add Webhook for archiving / restoring by @LarsK1 in #8216
* Minor cleanup for filters by @turbo124 in #8232
* Refactor for Observers by @turbo124 in #8237
* Enhance discount label if percentage (closes #8204) by @tissieres in #8226
* Optional user input after a quote is approved. by @turbo124 in #8242

[1.6.61]
* Update Invoice Ninja to 5.5.67
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.67)
* Fixes for using credits in subscriptions
* Add payment number when generating credit payment from subscription
* Minor fixes for quote zips
* Fixes for vendor imports

[1.6.62]
* Update Invoice Ninja to 5.5.69
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.69)
* Attach recurring invoice docs by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8258
* Feature: Webhook at send by @LarsK1 in https://github.com/invoiceninja/invoiceninja/pull/8245
* Design fixes by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8259
* Fixes for webhooks by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8261
* Bump predis to 2.x by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8262

[1.6.63]
* Update Invoice Ninja to 5.5.70
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.70)
* Fixes for translation string in subscriptions by @turbo124 in #8268
* Fixes for archived clients attempting to view client portal. by @turbo124 in #8269
* Fixes for job middleware by @turbo124 in #8270

[1.6.64]
* Update Invoice Ninja to 5.5.71
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.71)
* Our scheduler has now been released! You can currently use the scheduler to send Client Statements at predefined intervals with their own specific date ranges
* Fixes for base redirect
* Remove predis 2
* Clean up for access permissions
* Fixes for designs and client compatibility
* Update composer requirements
* Remove logo from CSV importer

[1.6.65]
* Update Invoice Ninja to 5.5.73
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.73)
* We have improved email button compatibility across email clients, users who use Outlook should now consistently see an appropriately sized button.
* Minor Fixes for some designs
* Significantly reduced the number of queued jobs when performing bulk actions
* Fixes for displaying negative currencies

[1.6.66]
* Update Invoice Ninja to 5.5.74
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.74)
* Add BankTransfers From Stripe as a new payment method
* Refactor Stripe description strings to use a common method.
* Update Stripe Alipay to use Payment Intents

[1.6.67]
* Update Invoice Ninja to 5.5.76
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.76)
* Streamline whitelabel logo styles in PdfMaker by @danielkoch in https://github.com/invoiceninja/invoiceninja/pull/8300
* Limit per page to 5000 by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8302
* Fix for expense <> transaction decoupling by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8304
* Add guards on invoice_id by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8305
* Update designs by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8315
* v5.5.76 by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8318

[1.6.68]
* Update Invoice Ninja to 5.5.78
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.78)

[1.6.69]
* Update Invoice Ninja to 5.5.79
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.79)
* Fixes for displaying negative numbers by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8324
* Add markdown support for subscriptions by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8325

[1.6.70]
* Update Invoice Ninja to 5.5.80
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.80)
* Fixes for oversized mailables

[1.6.71]
* Update Invoice Ninja to 5.5.81
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.81)
* Fixes for PaymentExport regression.

[1.6.72]
* Update Invoice Ninja to 5.5.82
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.82)
* New Flutter AP

[1.6.73]
* Update Invoice Ninja to 5.5.83
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.83)
* Support processing different delimiters for CSV imports by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8334
* Fixes for React Settings Cast by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8335
* Fixes for user stubs in random data by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8336
* Improve rounding for negative numbers by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8337
* Fixes for react settings by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8339
* v5.5.83 by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8340
* Fixes for client registration migration by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8341

[1.6.74]
* Update Invoice Ninja to 5.5.84
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.84)
* Email refactor by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8342

[1.6.75]
* Update Invoice Ninja to 5.5.86
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.86)
* Add missing migration by @turbo124 in #8348

[1.6.76]
* Update Invoice Ninja to 5.5.87
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.87)
* Create 2023_03_10_100629_add_currencies.php by @datasolutionsbz in https://github.com/invoiceninja/invoiceninja/pull/8352
* Update texts.php by @datasolutionsbz in https://github.com/invoiceninja/invoiceninja/pull/8351
* Updates for RandomDataSeeder by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8353
* Add notification label by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8354
* Stripe: Add BACS by @LarsK1 in https://github.com/invoiceninja/invoiceninja/pull/8079
* v5.5.87 by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8355
* Updates for JS bundles by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8356

[1.6.77]
* Update Invoice Ninja to 5.5.89
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.89)
* Prevent duplicate shipping address details on delivery notes by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8358
* v5.5.88 by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8359
* Minor formatting by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8360
* Fixes for Invoice Preview Designer
* Fixes for preloader - exclude testing frameworks by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8366
* v5.5.89 by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8371

[1.6.78]
* Update Invoice Ninja to 5.5.90
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.90)
* Filters for overdue invoices
* Additional defaults for primary colors
* Minor fixes for events for invoice was emailed
* Expand exception handling in Email class
* move NinjaUser to sync when creating accounts
* Clean up for Login Controller
* Clean up auth dir
* Fixes for sending from email name
* Fixes for correct token
* Stub translated labels for designer
* Fixes for company token sanity
* Fixes for AVS checks using Braintree
* Add logo class
* Update dependencies for js
* Add search by client from invoices
* Search by client name in entities
* Do not subtract services as product inventory
* Fixes for tests
* Allow removing deleted user
* Liap includes
* Updates for PDF designer with custom columns (API only)
* Add translated stubs for the PDF Mock
* Minor fixes for Mocks
* Recurring invoice update / upgrade pricing (API only)
* Recurring invoice price modifications (API only)
* Increase recurring prices (API only)

[1.6.79]
* Update Invoice Ninja to 5.5.92
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.92)
* Fixes for quotes with deleted contacts - download PDF failures
* Hide pre payments from client portal.

[1.6.80]
* Update Invoice Ninja to 5.5.93
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.93)

[1.6.81]
* Update Invoice Ninja to 5.5.94
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.94)
* Fixes for webhooks not sending user detfined headers
* Implement stubs for websockets
* Add invoices to recurring invoices in Client Portal.
* Stubs for Scheduling an invoice to be sent
* API implementation of Pre Payments completed
* Allow a CC email address to be passed into the Emailer via cc_email request parameter

[1.7.0]
* Update Invoice Ninja to 5.5.95
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.95)
* Websockets by @turbo124 in #8385
* Recurring invoice price modifications
* Translate labels for Invoice Designer
* Updates for PDF designer with custom columns
* Allow removing deleted user
* Do not subtract services as product inventory
* Search by client name in entities
* Add search by client from invoices
* Updates for company user / token checks
* Update dependencies for js
* Add logo class
* Fixes for AVS checks using Braintree
* Fixes for company token sanity

[1.7.1]
* Update Incoive Ninja to 5.5.96
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.96)
* Redis is now required
* cookie removed from echeck header api. by @kishanmnpatel in https://github.com/invoiceninja/invoiceninja/pull/8388
* Hide pre payments
* Minor changes for GoCardless
* Catch all logo
* Updates for favicon
* Fixes for invoice filters - overdue
* Schedule Entity
* Fixes for scheduler tests after refactor
* Update user input for po number when approving a quote
* Update quote input from notes to purchase order number
* Add Invoice Reference Subject
* Add tests for scheduling email entity
* Stub global tax rates

[1.7.2]
* Update Invoice Ninja to 5.5.97
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.97)
* Update production routes by @turbo124 in #8394
* Fixes for limiter by @turbo124 in #8396

[1.7.3]
* Update Invoice Ninja to 5.5.98
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.98)
* Change Pre Payments to be always available as recurring by @turbo124 in #8397
* Tasks (descriptions per line)
* Tasks (billable by line)
* Invoice / Quote PDF option for separate product columns
* Gocardless. Upgraded so that all payment methods / regions are now supported.

[1.7.4]
* Update Invoice Ninja to 5.5.99
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.99)
* Fixes for react builder by @turbo124 in #8399
* Allow custom designs to be passed into the live designer by @turbo124 in #8400
* Update webpack by @turbo124 in #8401
* Configurable MailGun endpoint by @turbo124 in #8402
* Fixes for Bank Transfers with Stripe

[1.7.5]
* Update Invoice Ninja to 5.5.101
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.101)
* Fixes for recurring price increases/updates by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8405
* Fixes for upcoming invoices filters by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8406
* Add translations by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8407
* Fixes for batch updating prices in recurring invoices

[1.7.6]
* Update Invoice Ninja to 5.5.102
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.102)
* Minor fixes for payment emailed activities by @turbo124 in #8413
* Fixes for View Recurring Invoice PDF

[1.7.7]
* Update Invoice Ninja to 5.5.103
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.103)
* Updated readme by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8421
* Set default tax id for products by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8425
* Fixes for migrations by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8426
* Coerce the string to array if empty string passed for design by @beganovich in https://github.com/invoiceninja/invoiceninja/pull/8410
* Add updated calculated fields cron by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8428
* Disconnect OAuth Mailer
* Add graceful way to handle cut/paste react links
* Refactor to allow link multiple expenses to a transaction
* Remove requirements for cache to be updated after .env vars are changed

[1.7.8]
* Update Invoice Ninja to 5.5.104
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.104)
* Fixes for linking expenses by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8430
* Payment token issue solved. by @kishanmnpatel in https://github.com/invoiceninja/invoiceninja/pull/8432
* Minor fixes by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8436
* Filtering by is_custom by @beganovich in https://github.com/invoiceninja/invoiceninja/pull/8433

[1.7.9]
* Update Invoice Ninja to 5.5.105
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.105)
* Fixes for logging by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8440
* Support for XRechnung / ZUGFeRD / e-Factur by @LarsK1 in https://github.com/invoiceninja/invoiceninja/pull/8368
* Additional translations by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8442
* Fixes for E-Invoice-Tests by @LarsK1 in https://github.com/invoiceninja/invoiceninja/pull/8444
* Recurring invoices filters by @talkstraightuk in https://github.com/invoiceninja/invoiceninja/pull/8447
* Updates for design filters by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8449
* Bulk route for products by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8451
* Minor fixes for translations by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8453
* Updated translations by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8454

[1.7.10]
* Update Invoice Ninja to 5.5.106
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.106)
* Facturae-Spain by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8456
* e-invoicing by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8457
* v5.5.106 by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8459

[1.7.11]
* Update Invoice Ninja to 5.5.107
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.107)
* Handle e-invoices when creating zip files by @turbo124 in #8466
* EInvoice ZuGFeRD: Make shipping address optional by @LarsK1 in #8467
* Fixes for reports by @turbo124 in #8468

[1.7.12]
* Update Invoice Ninja to 5.5.109
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.109)
* Improve Cancel URL experience with Paypal by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8471
* Update blacklist rules by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8472
* FIxes for show_credits_tables by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8473
* Minor fixes for browser caching by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8474

[1.7.13]
* Update Invoice Ninja to 5.5.110
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.110)
* Fix for regression by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8475

[1.7.14]
* Update Invoice Ninja to 5.5.112
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.112)
* e-invoices fixes.

[1.7.15]
* Update Invoice Ninja to 5.5.113
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.113)
* Fixes for ZuGFerd E-invoice by @LarsK1 in #8486
* Fixes for e-invoices by @turbo124 in #8487
* Minor fixes by @turbo124 in #8488

[1.7.16]
* Update Invoice Ninja to 5.5.114
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.114)
* Updated self updater to use .tar files instead of .zip
* Add group setting filters by @turbo124 in #8490

[1.7.17]
* Update Invoice Ninja to 5.5.115
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.115)
* Improvements for correct ZuGFerd tax calculations by @LarsK1 in https://github.com/invoiceninja/invoiceninja/pull/8495
* Log import array by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8499
* v5.5.115 by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8503
* Fixes for InvoiceWasViewed event

[1.7.18]
* Update Invoice Ninja to 5.5.116
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.116)
* Fixes for regression in Paypal Express driver.
* Add ability to sign e-invoice with SSL
* v5.5.116 by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8504

[1.7.19]
* Update Invoice Ninja to 5.5.117
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.117)
* ES E-Invoicing by @turbo124 in #8506
* Fix for gateway fees when using line item taxes
* Fixes for imports
* v5.5.117 by @turbo124 in #8507

[1.7.20]
* Update Invoice Ninja to 5.5.118
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.118)
* Fixes for chart queries by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8509
* Tax Tests by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8510
* Fixes for sync quote / invoice columns
* Fixes for e-invoicing FACe

[1.7.21]
* Update Invoice Ninja to 5.5.119
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.119)
* Minor fixes for ZuGFeRD by @LarsK1 in https://github.com/invoiceninja/invoiceninja/pull/8513
* v5.5.119 by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8515

[1.8.0]
* Update Invoice Ninja to 5.5.122
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.122)
* Update documents by @turbo124 in #8518
* Update documents / company settings by @turbo124 in #8519
* Fixes for `text_body` by @turbo124 in #8520
* Add expense categories if they do not exist on import by @turbo124 in #8522
* Add signatures on e-invoices.
* Fixes for Paytrace in dev mode
* Fixes for custom email templates

[1.8.1]
* Update Invoice Ninja to 5.5.123
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.123)
* Improve auto bill text in client portal by @turbo124 in #8525
* Fixes for client settings by @turbo124 in #8529
* Minor fixes by @turbo124 in #8530

[1.8.2]
* Update Invoice Ninja to 5.5.124
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.5.124)
* AT Tax enablement. by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8534

[1.9.0]
* Update Invoice Ninja to 5.6.0
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.0)
* Updates for auth.net and require cvv codes by @turbo124 in #8535
* GA of automated sales tax calculations
* Refactor for administrator URLs depending on which portal you are using React / Flutter
* Fixes for custom email templates
* Add phone to list of required fields for auth.net

[1.9.1]
* Update Invoice Ninja to 5.6.1
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.1)
* Password timeout route by @turbo124 in #8538

[1.9.2]
* Update Invoice Ninja to 5.6.2
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.2)
* fixes for react links by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8546
* v5.6.2 by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8553
* Add auth to document downloads by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8555

[1.9.3]
* Update Invoice Ninja to 5.6.3
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.3)
* Client and tasks includes for project by @beganovich in #8560
* Add "All Time" to scheduled statements date range; allow excluding clients with no matching statements by @joshuadwire in #8556

[1.9.4]
* Update Invoice Ninja to 5.6.4
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.4)
* Minor fixes by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8563

[1.9.5]
* Update Invoice Ninja to 5.6.5
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.5)
* General fixes and improvements by @turbo124 in #8575
* Minor fixes for bank transaction processing by @turbo124 in #8577
* Fixes for PDF Previews by @turbo124 in #8580
* v5.6.5 by @turbo124 in #8587
* Add :MONTHYEAR keyword to recurring expenses by @lamaral in #8578

[1.9.6]
* Update Invoice Ninja to 5.6.7
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.7)
* iDeal notifications by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8590
* minor fixes for constructor props by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8592
* Fixes for mailer by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8593
* More fixes for the ZUGFeRD implementation by @LarsK1 in https://github.com/invoiceninja/invoiceninja/pull/8599
* Fixes for duplicated XInvoice file attachments by @LarsK1 in https://github.com/invoiceninja/invoiceninja/pull/8601
* updates for user preferences route by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8602
* Updates for composer dependencies by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8604

[1.9.7]
* Update Invoice Ninja to 5.6.12
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.12)

[1.9.8]
* Update Invoice Ninja to 5.6.13
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.13)
* Updated translations
* Fixes for plain email template variable replacements

[1.9.9]
* Update Invoice Ninja to 5.6.14
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.14)
* Fixes for Square
* Signed URLs for Protected Downloads

[1.9.10]
* Update Invoice Ninja to 5.6.15
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.15)
* Fix for regression on rate limiting throttle by @turbo124 in #8624

[1.9.11]
* Update Invoice Ninja to 5.6.17
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.17)
* Fixes for CORS
* Remove eval()

[1.9.12]
* Update Invoice Ninja to 5.6.18
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.18)
* Fixes for protected downloads in Docker Containers by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8634
* Improve client portal when viewing on mobile

[1.9.13]
* Update Invoice Ninja to 5.6.19
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.19)
* Fixes for signatures / terms in Client Portal

[1.9.14]
* Update Invoice Ninja to 5.6.20
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.20)
* Updates for purging clients by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8651
* Adjustments for restoring a deleted invoice with a deleted payment by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8653
* Change length of Webhook Urls by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8656

[1.9.15]
* Update Invoice Ninja to 5.6.21
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.21)
* Updates for currencies by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8661
* Silence errors on company logo delete. by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8662

[1.9.16]
* Update Invoice Ninja to 5.6.22
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.22)
* Fixes for Signatures by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8668
* Fixes for e-invoice (Spanish) by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8669

[1.9.17]
* Update Invoice Ninja to 5.6.23
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.23)
* Fixes for reminders - timezone offsets by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8672

[1.9.18]
* Update Invoice Ninja to 5.6.24
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.24)
* Fixes for Recurring expense generation
* Minor fixes for inclusive tax calculations by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8680
* Minor fixes by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8681
* Addtional query filters for invoices
* Fixes for deleted payments
* Fixes for timezone corrections for reminders
* Fixes for permissions to view tax rates
* Fixes for recurring invoices
* Remove currency formatting of payments from exports
* Updates for e-invoice to include legal literals and also payments
* Set todays date on a new expense
* Fixes for signature dates and IP addresses

[1.9.19]
* Update Invoice Ninja to 5.6.25
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.25)
* Fixes for expense id encoding in bank transactions
* Update last sent date for recurring expenses
* Updates for license checks
* Minor fixes for inclusive tax calculations

[1.9.20]
* Update Invoice Ninja to 5.6.26
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.26)
* Fixes for static analysis by @turbo124 in #8692
* Fixes for unlinking company exports by @turbo124 in #8694
* Minor Fixes. by @turbo124 in #8695

[1.9.21]
* Update Invoice Ninja to 5.6.27
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.27)
* Fixes for quote approval

[1.9.22]
* Update Invoice Ninja to 5.6.28
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.28)
* Currencies: XAG, XAU, and missing name for LYD by @tctlrd in #8698
* xau,xag currencies by @turbo124 in #8703
* Fixes for currency seeder ids by @turbo124 in #8704
* Fixes for translations file by @turbo124 in #8705
* Fixes for Spanish E-Invoicing

[1.9.23]
* Update Invoice Ninja to 5.6.30
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.29)
* Update texts for tax info placeholders
* Updates for tests for html generation
* Updates for tests for html generation with special translations
* Fixes for yodlee
* Additional currencies
* Tests for adding language ID for vendors
* Tests for account summary DTO
* Updates for yodlee account status
* Add last_login timestamps for vendor contacts and vendors
* Add events for catching contact logins
* Support using draft credits in payments
* Adjustments for Facturae 3.2.2

[1.9.24]
* Update Invoice Ninja to 5.6.31
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.6.31)
* Adjust xml-generation for E-Invoicing by @LarsK1 in #8697
* Adjustments e-invoicing by @turbo124 in #8722

[1.10.0]
* Update Invoice Ninja to 5.7.0
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.0)
* Fixes for currency seeder ids by @turbo124 in #8704
* Fixes for translations file by @turbo124 in #8705
* Adjust xml-generation for E-Invoicing by @LarsK1 in #8697
* Refactor for e-invoicing by @turbo124 in #8722
* Report Previews for Credits by @turbo124 in #8728
* Fixes for report previews by @turbo124 in #8730
* Fixes for collection to array conversion. by @turbo124 in #8731
* Fixes for report previews by @turbo124 in #8732
* Laravel 10 by @turbo124 in #8733
* Adjustment for Purchase Orders in Portal by @turbo124 in #8734

[1.10.1]
* Update Invoice Ninja to 5.7.4
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.4)
* Laravel10 by @turbo124 in #8739

[1.10.2]
* Update Invoice Ninja to 5.7.5
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.5)
* Bug fix for db::raw in `payment_balance` query

[1.10.3]
* Update Invoice Ninja to 5.7.6
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.6)
* Updates for credit reports by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8748
* Report Previews by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8759
* Updates for github issue templates by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8763
* Adjustments for invoice calculations to support higher precision by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8764

[1.10.4]
* Update Invoice Ninja to 5.7.7
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.7)
* Improve Tax Summary Reports
* Fixes for DB::raw() queries

[1.10.5]
* Update Invoice Ninja to 5.7.8
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.8)
* Removes all references to storage paths for PDFs, all PDFs are now dynamincally generated on demand
* Refactor for Bulk zip
* Refactor for PDF Merge

[1.10.6]
* Update Invoice Ninja to 5.7.9
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.9)
* Fixes for check data script

[1.10.7]
* Update Invoice Ninja to 5.7.10
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.10)

[1.10.8]
* Update Invoice Ninja to 5.7.11
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.11)
* added void transaction support for auth.net

[1.10.9]
* Update Invoice Ninja to 5.7.12
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.12)
* Updates for search + fixes for gross line totals by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8797

[1.10.10]
* Update Invoice Ninja to 5.7.13
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.13)
* Checks for missing props by @turbo124 in #8806

[1.10.11]
* Update Invoice Ninja to 5.7.14
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.14)
* Import Company Model and Hash in usersTableSeeder.php by @chandachewe10 in #8794

[1.10.12]
* Update Invoice Ninja to 5.7.15
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.15)
* Prioritize principle name for microsoft authentication

[1.10.13]
* Update Invoice Ninja to 5.7.17
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.17)
* Updates for search

[1.10.14]
* Update Invoice Ninja to 5.7.22
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.22)
* Changes for react build by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8818
* Fixes for react builds by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8820

[1.10.15]
* Update Invoice Ninja to 5.7.24
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.24)
* Prevent duplicate tax names by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8822
* Minor updates + translations by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8826
* Static analysis clean up by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8827
* Updates for searching report previews by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8828
* Migration to Vite by @beganovich in https://github.com/invoiceninja/invoiceninja/pull/8778
* Check out only the v5-stable branch by @NucleaPeon in https://github.com/invoiceninja/invoiceninja/pull/8831
* Update build files for vite by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8832

[1.10.16]
* Update Invoice Ninja to 5.7.25
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.25)
* Improve resiliency of pdf previews by @turbo124 in #8843
* Fixes for card-js by @beganovich in #8847

[1.10.17]
* Update Invoice Ninja to 5.7.26
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.26)

[1.10.18]
* Update Invoice Ninja to 5.7.27
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.27)
* Return correct identifier for reports by @beganovich in https://github.com/invoiceninja/invoiceninja/pull/8860
* Fixes for public/private documents by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8861

[1.10.19]
* Update Invoice Ninja to 5.7.28
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.28)

[1.10.20]
* Update Invoice Ninja to 5.7.29
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.29)

[1.10.21]
* Update Invoice Ninja to 5.7.30
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.30)
* Fixes for RandomDataSeeder by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8869
* FIxes for Trials in subscriptions

[1.10.22]
* Update Invoice Ninja to 5.7.31
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.31)
* Updates for missing props by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8875
* Fixes for missing Tax_id prop by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8876
* Minor fixes for paytrace by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8877
* Fixes for Swiss QR Codes by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8885

[1.10.23]
* Update Invoice Ninja to 5.7.32
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.32)
* Refactor for live previews.

[1.10.24]
* Update Invoice Ninja to 5.7.33
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.33)
* Fixes for EPC / SEPA QR Codes

[1.10.25]
* Update Invoice Ninja to 5.7.34
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.34)
* Align client export with rest of code by @beganovich in https://github.com/invoiceninja/invoiceninja/pull/8891
* Ensure order of Item exports by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8893
* Add client.number to maps by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8895
* Add support for Libre Office by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8902
* Fixes for QR code by @beganovich in https://github.com/invoiceninja/invoiceninja/pull/8906

[1.10.26]
* Update Invoice Ninja to 5.7.35
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.35)
* Add "uninvoiced" Task Filter by @gescarra in https://github.com/invoiceninja/invoiceninja/pull/8913

[1.10.27]
* Update Invoice Ninja to 5.7.36
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.36)
* Fixes for Flutter web application not rendering

[1.10.28]
* Update Invoice Ninja to 5.7.37
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.37)
* Fixes for variables replacements

[1.10.29]
* Update Invoice Ninja to 5.7.38
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.38)

[1.10.30]
* Update Invoice Ninja to 5.7.39
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.39)

[1.10.31]
* Update Invoice Ninja to 5.7.42
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.42)

[1.10.32]
* Update Invoice Ninja to 5.7.44
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.44)
* Fixes for missing `vendor_contacts()` during migration

[1.10.33]
* Update Invoice Ninja to 5.7.45
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.45)
* Fixes for e invoices

[1.10.34]
* Update Invoice Ninja to 5.7.46
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.46)

[1.10.35]
* Update Invoice Ninja to 5.7.47
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.47)
* Fixes for task id tax display by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8959
* Minor fixes by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8960
* Fixes for system maintenance by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8962

[1.10.36]
* Update Invoice Ninja to 5.7.48
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.48)

[1.10.37]
* Update Invoice Ninja to 5.7.50
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.50)
* Fixes for timezone offsets

[1.10.38]
* Update Invoice Ninja to 5.7.51
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.51)
* Rollback for token auth on pdf routes
* Fixes for send time using timezone offset calculations
* Fixes for custom product quote columns

[1.10.39]
* Update Invoice Ninja to 5.7.52
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.52)
* Adjustment for missing props by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8903
* Fixes the reference field to be filled when scanning QR code with ban… by @sideonshore in https://github.com/invoiceninja/invoiceninja/pull/8972
* Add link to invoice - if it exists by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8984

[1.10.40]
* Update Invoice Ninja to 5.7.54
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.54)
* Translation for line item by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8989

[1.10.41]
* Update Invoice Ninja to 5.7.55
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.55)
* PayPal PPCP by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8994
* Add domain notifications by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8996
* Feature: Add new ZuGFerD standards by @LarsK1 in https://github.com/invoiceninja/invoiceninja/pull/8997
* Stubs for extensions. by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8998
* Fixes for translation arrays by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/8999

[1.10.42]
* Update Invoice Ninja to 5.7.57
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.57)
* Add margin-top to public_notes on clean design by @timconner in https://github.com/invoiceninja/invoiceninja/pull/9005
* Added $balance_due_dec as usable variable by @kenny665 in https://github.com/invoiceninja/invoiceninja/pull/8950
* HOTFIX: en_GB language is broken by @fidelix in https://github.com/invoiceninja/invoiceninja/pull/9010
* Update project filters to allow filtering and sorting by client by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9011

[1.10.43]
* Update Invoice Ninja to 5.7.58
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.58)
* Additional check data methods
* Fixes for statements preventing a default invoice PDF being sent
* Add account relation for activity table
* Disable checkout.com pay now buttons on submit.
* Fixes for past due/upcoming invoice filters
* Fixes for redirecting on successful subscription purchase

[1.10.44]
* Update Invoice Ninja to 5.7.59
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.59)
* Streamline whitelabel logo styles in pdf builder by @danielkoch in https://github.com/invoiceninja/invoiceninja/pull/9027

[1.10.45]
* Update Invoice Ninja to 5.7.61
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.61)
* Add case for transaction searches
* Add checks for subdomain validation
* Set appropriate date format for payment factory
* Improvements for upcoming filters
* Fixes for import validation
* Update request rules for imports
* Updates for readme
* Fixes for report names
* Fixes for GoCardless Instant Bank Payments
* Fixees for numbering when set to When Sent

[1.10.46]
* Update Invoice Ninja to 5.7.62
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.62)

[1.10.47]
* Update Invoice Ninja to 5.7.63
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.7.63)
* Add php 8.3 testing by @heddn in https://github.com/invoiceninja/invoiceninja/pull/9047
* Update blacklist rules by @joecool1029 in https://github.com/invoiceninja/invoiceninja/pull/9046
* Updates for readme by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9051

[1.11.0]
* Update Invoice Ninja to 5.8.0
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.0)
* Feature: nordigen bank integration by @paulwer in https://github.com/invoiceninja/invoiceninja/pull/9004
* Nordigen / GoCardless updates. by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9074
* Livewire v3 upgrade by @jasonlbeggs in https://github.com/invoiceninja/invoiceninja/pull/9041
* Fixes for prepayments by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9099
* Renaming comment in Kernel.php to "Expenses" and not "Invoices". by @JasmeowTheCat in https://github.com/invoiceninja/invoiceninja/pull/9098

[1.11.1]
* Update Invoice Ninja to 5.8.1
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.1)

[1.11.2]
* Update Invoice Ninja to 5.8.2
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.2)
* Reset counters at client and group levels by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9103

[1.11.3]
* Update Invoice Ninja to 5.8.3
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.3)
* Fixes for Partial due date type in InvoiceTransformer

[1.11.4]
* Update Invoice Ninja to 5.8.8
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.8)

[1.11.5]
* Update Invoice Ninja to 5.8.9
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.9)

[1.11.6]
* Update Invoice Ninja to 5.8.10
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.10)
* Fixes for links to forum in setup. by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9145
* fix: nordigen transaction id should be string by @paulwer in https://github.com/invoiceninja/invoiceninja/pull/9134
* Lao currency + lang
* Updated translations
* Fixes for Purchase order previews
* Fixes for Recurring Inovice statuses
* Float parsing on imports

[1.11.7]
* Update Invoice Ninja to 5.8.11
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.11)
* Livewire bundling by @turbo124 in #9147
* Add mappings for participant and participant_name by @turbo124 in #9149

[1.11.8]
* Update Invoice Ninja to 5.8.12
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.12)
* fix: nordigen amount imported as absolute by @paulwer in #9150
* Partial Due Date valudation by @turbo124 in #9156
* v5.8.12 by @turbo124 in #9165
* Feature: nordigen additional transaction info in description by @paulwer in #9157

[1.11.9]
* Update Invoice Ninja to 5.8.14
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.14)
* Fixes for mailables. by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9175
* Fix: nordigen transaction import error by @paulwer in https://github.com/invoiceninja/invoiceninja/pull/9180
* Add Net / tax amounts to expense reports by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9181

[1.11.10]
* Update Invoice Ninja to 5.8.15
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.15)

[1.11.11]
* Update Invoice Ninja to 5.8.16
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.16)
* E-mail preferences links by @beganovich in https://github.com/invoiceninja/invoiceninja/pull/9172
* Prevent reminder fees being taxes in DE region by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9191
* Fixes for check data
* API Doc update
* Raw QR codes

[1.11.12]
* Update Invoice Ninja to 5.8.17
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.17)
* Fixes for Nordigen by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9196
* FIX: Nordigen Import Error, when missing transactionId from instituition by @paulwer in https://github.com/invoiceninja/invoiceninja/pull/9202
* Allow Gocardless ACSS payments for CAD customers by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9203

[1.11.13]
* Update Invoice Ninja to 5.8.18
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.18)
* Adjustments for logic for sending payment emails by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9209
* Replace variables in Terms for user acceptance in Client Portal by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9210

[1.11.14]
* Update Invoice Ninja to 5.8.19
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.19)

[1.11.15]
* Update Invoice Ninja to 5.8.20
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.20)
* Send users to overview page instead of edit for React by @turbo124 in #9214
* Updates for @Isset() by @turbo124 in #9220
* Updated translations
* Send users to overview page instead of edit for React
* Fixes for payment emails

[1.11.16]
* Update Invoice Ninja to 5.8.21
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.21)
* Fixes for plain templates.

[1.11.17]
* Update Invoice Ninja to 5.8.22
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.22)
* Fixes for email templates
* Fixes for migration scripts

[1.11.18]
* Update Invoice Ninja to 5.8.23
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.23)
* Required form fields by @beganovich in https://github.com/invoiceninja/invoiceninja/pull/9224
* Fixes for RFF by @beganovich in https://github.com/invoiceninja/invoiceninja/pull/9243
* Fixes for email reports by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9244
* RFF validation errors on invoice show page by @beganovich in https://github.com/invoiceninja/invoiceninja/pull/9245
* Fixes for edge cases in reports by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9248

[1.11.19]
* Update Invoice Ninja to 5.8.24
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.24)
* Improve Recurring Expense Filters by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9266
* Fixes for tax_amount when migrating by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9267
* RFF support for purchase links by @beganovich in https://github.com/invoiceninja/invoiceninja/pull/9273
* Pro Rata subscriptions calculation by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9278
* Fix openAPI specification build order by @Flole998 in https://github.com/invoiceninja/invoiceninja/pull/9283
* Switching subscriptions RFF by @beganovich in https://github.com/invoiceninja/invoiceninja/pull/9285
* Fix missing / in openapi-docs by @Flole998 in https://github.com/invoiceninja/invoiceninja/pull/9276
* Fix various openAPI related mistakes in openapi/paths.yaml by @Flole998 in https://github.com/invoiceninja/invoiceninja/pull/9275
* Adjustments for task imports by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9286
* Nordigen API fix: iban should be treated as optional by @kevinpetit in https://github.com/invoiceninja/invoiceninja/pull/9289
* Fixes for openapi spec by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9291

[1.11.20]
* Update Invoice Ninja to 5.8.25
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.25)
* Set default payment notifications for new companies by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9298

[1.11.21]
* Update Invoice Ninja to 5.8.26
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.26)
* Add in company columns for SMTP configuration
* Add Smtp check route
* Adjustments for test mail server
* Fixes for parse float
* Updates for company factory
* Improve PDF Mock
* Adjustments for eager loading
* Enforce columns that we can sort on

[1.11.22]
* Update Invoice Ninja to 5.8.27
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.27)
* Fixes for SMTP port type

[1.12.0]
* Disable the internal queue

[1.12.1]
* Update Invoice Ninja to 5.8.28
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.28)
* Feat: translations for nordigen transaction transformer - exchange rate by @paulwer in https://github.com/invoiceninja/invoiceninja/pull/9304
* Fixes for company export paths by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9323
* Updated translations
* Fixes for user filter sort by
* Adjustments for invoicesum
* Updates for exchange rates
* Updated 2FA error message response
* Updates for type for smtp port

[1.12.2]
* Update Invoice Ninja to 5.8.29
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.29)

[1.12.3]
* Update Invoice Ninja to 5.8.30
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.30)
* Fixes for payment filters

[1.12.4]
* Update Invoice Ninja to 5.8.31
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.31)
* Updates for smtp configuration
* Updates for parseformat
* Remove redundant include
* Better handling of failsafe for US State calculations
* Fixes for payment filters
* Set verify peer - default = true

[1.12.5]
* Update Invoice Ninja to 5.8.32
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.32)
* Fixes for tax name equal to or less than 2 characters

[1.12.6]
* Update Invoice Ninja to 5.8.33
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.33)

[1.12.7]
* Update Invoice Ninja to 5.8.34
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.34)

[1.12.8]
* Update Invoice Ninja to 5.8.35
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.35)
* Updated translations and fixes for webhooks. by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9359
* allow plural for report names in scheduler by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9360
* Handle edge case with Nordigen where account appears active by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9362

[1.12.9]
* Update Invoice Ninja to 5.8.36
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.36)
* Fix regression in webhooks
* Fixes for SMTP test sending
* Fixes for parseFloat

[1.12.10]
* Update Invoice Ninja to 5.8.37
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.37)
* Fixes for updating client record

[1.12.11]
* Do not run scheduler and queue at the same time

[1.12.12]
* Update Invoice Ninja to 5.8.38
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.38)
* Feature: Brevo Email Provider by @paulwer in https://github.com/invoiceninja/invoiceninja/pull/9053
* Gateway authentication by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9385
* Import customers from all gateways by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9387
* Feature: Allow more document types for E-Invoicing by @LarsK1 in https://github.com/invoiceninja/invoiceninja/pull/9364
* Fixes for payment validation by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9392
* Fixes for tests / name spaces - e-invoicing by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9395
* Client dashboard by @beganovich in https://github.com/invoiceninja/invoiceninja/pull/9397
* Fixes for RFF on direct payment page by @beganovich in https://github.com/invoiceninja/invoiceninja/pull/9394
* Updates for expense conversion from purchase order by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9402

[1.12.13]
* Bring back the scheduler task

[1.12.14]
* Update Invoice Ninja to 5.8.39
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.39)
* FIX: missing nordigen account disabled email on BankIntegrationController by @paulwer in https://github.com/invoiceninja/invoiceninja/pull/9406
* Add client filters to exports by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9408
* Control client dashboard using setting by @beganovich in https://github.com/invoiceninja/invoiceninja/pull/9405

[1.12.15]
* Update Invoice Ninja to 5.8.40
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.40)
* Subscriptions v3 by @beganovich in https://github.com/invoiceninja/invoiceninja/pull/9280
* Bulk assignment of clients to a group by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9425
* Improvements for templates & Minor bug fixes by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9429

[1.12.16]
* Update Invoice Ninja to 5.9.41
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.41)
* Updates for react build steps.

[1.12.17]
* Update Invoice Ninja to 5.9.43
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.43)

[1.12.18]
* Update Invoice Ninja to 5.9.45
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.45)

[1.12.19]
* Update Invoice Ninja to 5.9.46
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.46)

[1.12.20]
* Update Invoice Ninja to 5.9.47
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.47)

[1.12.21]
* Update Invoice Ninja to 5.9.48
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.48)
* FatturaPA by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9454

[1.12.22]
* Update Invoice Ninja to 5.9.49
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.49)
* Updated translations by @turbo124 in #9457
* Updates for subscriptions by @turbo124 in #9458
* Rollback invoice requests by @turbo124 in #9459

[1.12.23]
* Update Invoice Ninja to 5.8.50
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.50)

[1.12.24]
* Update Invoice Ninja to 5.8.51
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.51)

[1.12.25]
* Update Invoice Ninja to 5.8.52
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.52)
* Fix secondary fonts for Invoice Design preview by @dshoreman in #9480

[1.12.26]
* Update Invoice Ninja to 5.8.53
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.53)

[1.12.27]
* Updated Invoice Ninja to 5.8.54
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.54)
* Fixes for terms on subscirptions v3 by @beganovich in https://github.com/invoiceninja/invoiceninja/pull/9504
* Cleanup for Stripe webhooks. by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9507
* Fixes for gateway fields override by @beganovich in https://github.com/invoiceninja/invoiceninja/pull/9511
* Fixes for client filters for synthetic props. by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9512
* Redirect client to dashboard at login if enabled by @CarrnellTech in https://github.com/invoiceninja/invoiceninja/pull/9501

[1.12.28]
* Updated Invoice Ninja to 5.8.55
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.55)
* Task rounding implemented
* Refactor for start / end of quarters @turbo124 in #9514
* Version 5.8.55 by @turbo124 in #9518
* Redirect client to dashboard at login if enabled -fix #9417 #9501 by @CarrnellTech in #9516

[1.12.29]
* Updated Invoice Ninja to 5.8.56
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.56)

[1.12.30]
* Update Invoice Ninja to 5.8.57
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.8.57)
* Add twig security policy by default by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9525

[1.13.0]
* Update Invoice Ninja to 5.9.1
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.9.0)
* Update PHP to 8.3
* Add BTCPay payment capabilities by @Nisaba in #9520
* Updates for einvoicing schemas/models by @turbo124 in #9533
* Update PaymentLibrariesSeeder for BTCPay by @Nisaba in #9534
* Fixes for XInvoice by @LarsK1 in #9523
* Updates for BTC payment driver by @turbo124 in #9535
* Fix: Update Nordigen to properly handle small money transfer amounts by @yannickboy15 in #9537
* Update PayPal integration to support advanced cards by @turbo124 in #9550
* Update labels and fix custom company values in Invoice Preview by @dshoreman in #9556
* Updates for currencies by @turbo124 in #9565
* Add twig security policy by default by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9525

[1.13.1]
* Update Invoice Ninja to 5.9.2
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.9.2)
* Fixes for BTCpay

[1.13.2]
* Update Invoice Ninja to 5.9.3
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.9.3)
* Fixes a bug causing reminders not to send for users who have lock_invoices option turned on.

[1.13.3]
* Update Invoice Ninja to 5.9.4
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.9.4)

[1.13.4]
* Update Invoice Ninja to 5.9.5
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.9.5)

[1.13.5]
* Update Invoice Ninja to 5.9.6
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.9.6)

[1.13.6]
* Update Invoice Ninja to 5.9.8
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.9.8)

[1.13.7]
* Update Invoice Ninja to 5.9.9
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.9.9)

[1.14.0]
* Update Invoice Ninja to 5.10.2
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.2)

[1.14.1]
* Fix PDF generation

[1.14.2]
* Update Invoice Ninja to 5.10.3
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.3)

[1.14.3]
* Fix sending of emails with attachments

[1.14.4]
* Update Invoice Ninja to 5.10.4
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.4)

[1.14.5]
* Update Invoice Ninja to 5.10.5
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.5)

[1.14.6]
* Update Invoice Ninja to 5.10.6
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.6)

[1.14.7]
* Update Invoice Ninja to 5.10.7
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.7)

[1.14.8]
* Update Invoice Ninja to 5.10.8
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.8)

[1.14.9]
* Update Invoice Ninja to 5.10.9
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.9)

[1.14.10]
* Update Invoice Ninja to 5.10.10
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.10)

[1.14.11]
* Update Invoice Ninja to 5.10.11
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.11)

[1.14.12]
* Update Invoice Ninja to 5.10.12
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.12)

[1.14.13]
* Update Invoice Ninja to 5.10.13
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.13)

[1.14.14]
* Update Invoice Ninja to 5.10.16
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.16)

[1.14.15]
* Update Invoice Ninja to 5.10.18
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.18)
* Fixes for translations in client portal by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9857
* Remove migration for tax models by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9858
* Updated CSS by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9859
* Updated CSS by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9860
* Fixes for recurring invoice queries by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9865

[1.14.16]
* Update Invoice Ninja to 5.10.19
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.19)
* Fixes for subscriptions by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9874
* Minor fixes by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9875

[1.14.17]
* Update Invoice Ninja to 5.10.20
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.20)
* Fixes for EU tax calculations
* Expense category filters
* Add logic to support `task.is_running`
* Improvements for twig and special character encoding
* Improvements for PayPal Rest integration
* Fixes for Subscriptions displaying unrelated plans
* Updated chart queries

[1.14.18]
* Update Invoice Ninja to 5.10.21
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.21)
* Rate limit invoice actions
* Improve gateway fee confirmation
* Updated mail history queries
* Small fixes for payment email logic

[1.14.19]
* Update Invoice Ninja to 5.10.22
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.22)

[1.14.20]
* Update Invoice Ninja to 5.10.23
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.23)

[1.14.21]
* Update Invoice Ninja to 5.10.24
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.24)

[1.14.22]
* Update Invoice Ninja to 5.10.25
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.25)
* Fixes for ZUGFeRD by @LarsK1 in https://github.com/invoiceninja/invoiceninja/pull/9914
* Update translations by @beganovich in https://github.com/invoiceninja/invoiceninja/pull/9912
* Fix for calculate taxes with Recurring Invoices by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9918
* Purchase Order Accept Webhook by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9923
* Feature/import quickbooks by @karneaud in https://github.com/invoiceninja/invoiceninja/pull/9917
* Translations by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9924
* feat: Add YunoHost to self-hosted options by @CodeShakingSheep in https://github.com/invoiceninja/invoiceninja/pull/9931
* Fixes for inter EU tax rules by @turbo124 in https://github.com/invoiceninja/invoiceninja/pull/9934

[1.14.23]
* Update Invoice Ninja to 5.10.26
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.26)
* Fixes for custom fields that use switches - ensure output = yes/no

[1.14.24]
* Update Invoice Ninja to 5.10.27
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.27)
* Sort filters for `expense.payment_date` by @turbo124 in #9942
* Rotessa: New payment flow by @beganovich in #9945
* Minor fixes. by @turbo124 in #9948
* Add reversal for failed BTC payments by @turbo124 in #9960
* Updates for template service properties
* Fixes for confirming gateway fees
* Set blank string for signature date
* Remove paypal express, omnipay

[1.14.25]
* Update Invoice Ninja to 5.10.29
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.29)

[1.14.26]
* Update Invoice Ninja to 5.10.30
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.30)
* WebSockets: Add broadcast events by @beganovich in #10005
* Add vi lang by @turbo124 in #10027
* New payment flow: PDF on the show page by @beganovich in #10035
* GoCardless: OAuth by @beganovich in #9584
* Update translations by @beganovich in #10041
* GoCardless: Force sandbox for specific company by @beganovich in #10042

[1.14.27]
* Update Invoice Ninja to 5.10.31
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.31)
* Merge Vendors
* Add tests for bulk updates for expenses
* Add Bulk Updates
* Roll back schema
* Disable the scout driver
* minor fixes for Mollie

[1.14.28]
* Update Invoice Ninja to 5.10.32
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.32)
* Update API docs by @turbo124 in #10083
* Switch order of commands for PDF generation by @turbo124 in #10085
* Fixes for migrations by @turbo124 in #10087
* updates for composer.json by @turbo124 in #10088

[1.14.29]
* Update Invoice Ninja to 5.10.34
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.34)
* Add Taxation for Kleinunternehmerreglung and minor fixes by @​LarsK1 in https://github.com/invoiceninja/invoiceninja/pull/10082
* Fixes for static analysis by @​turbo124 in https://github.com/invoiceninja/invoiceninja/pull/10099
* Fixes for spacing in notes section by @​turbo124 in https://github.com/invoiceninja/invoiceninja/pull/10100 

[1.14.30]
* Update Invoice Ninja to 5.10.35
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.35)
* Fixes for task exports by @​turbo124 in https://github.com/invoiceninja/invoiceninja/pull/10115

[1.14.31]
* Update Invoice Ninja to 5.10.36
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.36)
* Fixes for running template actions on Tasks and Projects

[1.14.32]
* Update Invoice Ninja to 5.10.38
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.38)

[1.14.33]
* Update Invoice Ninja to 5.10.39
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.39)

[1.14.34]
* Update Invoice Ninja to 5.10.40
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.40)

[1.14.35]
* Update Invoice Ninja to 5.10.41
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.41)

[1.14.36]
* Update Invoice Ninja to 5.10.42
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.42)

[1.14.37]
* Update Invoice Ninja to 5.10.43
* [Full changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.43)

[1.14.38]
* Update invoiceninja to 5.10.44
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.44)
* Update variables for quotes / credits in templates by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10194
* Add e_invoicing_token to fillable  by [@&#8203;beganovich](https://github.com/beganovich) in https://github.com/invoiceninja/invoiceninja/pull/10184
* Updates for client props by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10196
* Updates for transforming PEPPOL by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10215
* Updated translations by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10216
* Update translations by [@&#8203;beganovich](https://github.com/beganovich) in https://github.com/invoiceninja/invoiceninja/pull/10210
* Preselecting token when available by [@&#8203;beganovich](https://github.com/beganovich) in https://github.com/invoiceninja/invoiceninja/pull/10191
* Improve helper text for stripe payment methods by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10226
* v5.10.44 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10230

[1.14.39]
* Update invoiceninja to 5.10.45
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.45)
* Fixes for Twig Security Policies
* Implementation of additional check data cli argument for client payment_balance
* Fixes for XRechnung xml files imports (now support CII + UBL versions)
* Implementation of xslt stylesheet formatting for e-invoicing validation + html display
* Currency Translation Fixes by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10231
* v5.10.45 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10235

[1.14.40]
* Update invoiceninja to 5.10.46
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.46)

[1.14.41]
* Update invoiceninja to 5.10.48
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.48)
* Improvements for payment token display (and preselection)
* Minor fixes for Forte by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10246
* v5.10.48 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10247
* Improve compatibility with CIUS XRechnung schema by [@&#8203;felixfischer](https://github.com/felixfischer) in https://github.com/invoiceninja/invoiceninja/pull/10244
* v5.10.47 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10245
* [@&#8203;felixfischer](https://github.com/felixfischer) made their first contribution in https://github.com/invoiceninja/invoiceninja/pull/10244

[1.14.42]
* Update invoiceninja to 5.10.49
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.49)
* Significantly improved Subscription UI/UX v2 + v3
* Fixes for Livewire data components
* Purchase order inventory cost => product cost
* v5.10.49 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10256

[1.14.43]
* Update invoiceninja to 5.10.50
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.50)
* Fixes for translation resources
* v5.10.50 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10258
* Fix [#&#8203;10241](https://github.com/invoiceninja/invoiceninja/issues/10241) (wrong item net prices in XInvoices) by [@&#8203;felixfischer](https://github.com/felixfischer) in https://github.com/invoiceninja/invoiceninja/pull/10257

[1.14.44]
* Update invoiceninja to 5.10.51
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.51)
* Significant improvements for handling Nordigen failures where ratelimiting exceptions resulted in requisition refreshes incorrectly.
* Vendor Portal fixes (Profile edits)
* v5.10.51 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10259
* Updated translations by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10260

[1.14.45]
* Update invoiceninja to 5.10.52
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.52)
* Fixes inappropriate route calls
* v5.10.52 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10261

[1.14.46]
* Update invoiceninja to 5.10.53
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.53)
* Improve return types on document submission by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10267

[1.14.47]
* Update invoiceninja to 5.10.54
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.54)
* E-invoicing: Remove additional tax identifiers by [@&#8203;beganovich](https://github.com/beganovich) in https://github.com/invoiceninja/invoiceninja/pull/10275
* E-invoicing: Fixes for acting validators by [@&#8203;beganovich](https://github.com/beganovich) in https://github.com/invoiceninja/invoiceninja/pull/10272
* E-invoicing: Validation for trying to add same country by [@&#8203;beganovich](https://github.com/beganovich) in https://github.com/invoiceninja/invoiceninja/pull/10274
* E-invoicing: Improve tax rate settings by [@&#8203;beganovich](https://github.com/beganovich) in https://github.com/invoiceninja/invoiceninja/pull/10273
* E-invoice: Removing tax identifiers fixes by [@&#8203;beganovich](https://github.com/beganovich) in https://github.com/invoiceninja/invoiceninja/pull/10277
* Update translations by [@&#8203;beganovich](https://github.com/beganovich) in https://github.com/invoiceninja/invoiceninja/pull/10278
* Check for emails prior to attempting a send by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10279
* Selfhosted Peppol documents by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10280

[1.14.48]
* Update invoiceninja to 5.10.55
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.55)
* Custom Payment Means for Zugferd documents
* Sync updating local quota value for self-hosted by [@&#8203;beganovich](https://github.com/beganovich) in https://github.com/invoiceninja/invoiceninja/pull/10301
* v5.10.55 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10303

[1.14.49]
* Update invoiceninja to 5.10.57
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.57)
* Reduce react js bundle to a single file

[1.14.50]
* Update invoiceninja to 5.10.58
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.58)
* Coalesce reminder resolution for invoices and quotes
* Fixes for ubl country
* Updated Translations
* Adjustments for contact registration (Cloudflare Turnstil implemented)
* Quote Conversion doc path fix. by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10318
* v5.10.58 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10320

[1.14.51]
* Update invoiceninja to 5.10.59
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.59)
* v5.10.59 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10337

[1.14.52]
* Update invoiceninja to 5.10.60
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.60)
* Updating Tax Model by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10339
* Update: Update Blockonomics driver by [@&#8203;cnohall](https://github.com/cnohall) in https://github.com/invoiceninja/invoiceninja/pull/10308
* Fixes for mail - scheme - symfony mailer by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10346
* Fixes for client portal not resolving
* Add cloudflare turnstile to protect /client/register routes

[1.14.53]
* Update invoiceninja to 5.10.61
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.61)
* Rollback Symfony Mailer by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10349

[1.14.54]
* Update invoiceninja to 5.10.62
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.10.62)
* Fixes for a regression with client statements
* v5.10.62 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10352

[1.15.0]
* Update invoiceninja to 5.11.0
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.0)
* einvoicing over PEPPOL network.
* Additional validation rules for custom surcharges by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10355
* v5.11.0 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10360

[1.15.1]
* Update invoiceninja to 5.11.1
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.1)
* Fixes for merge pdf by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10362
* Notifications: Generic message by [@&#8203;beganovich](https://github.com/beganovich) in https://github.com/invoiceninja/invoiceninja/pull/10361
* Fixes for purchase order designs by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10380
* Further improvements for public notifications by [@&#8203;beganovich](https://github.com/beganovich) in https://github.com/invoiceninja/invoiceninja/pull/10367
* Fix inbound issues by [@&#8203;paulwer](https://github.com/paulwer) in https://github.com/invoiceninja/invoiceninja/pull/10392
* Update translations by [@&#8203;beganovich](https://github.com/beganovich) in https://github.com/invoiceninja/invoiceninja/pull/10384
* v5.11.1 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10402
* Update translations by [@&#8203;beganovich](https://github.com/beganovich) in https://github.com/invoiceninja/invoiceninja/pull/10399
* Nordigen fixes by [@&#8203;dshoreman](https://github.com/dshoreman) in https://github.com/invoiceninja/invoiceninja/pull/10401

[1.15.2]
* Update invoiceninja to 5.11.2
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.2)
* Fixes for login regression
* v5.11.2 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10404

[1.15.3]
* Update invoiceninja to 5.11.3
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.3)
* Fixes for missing translations in the react application
* v5.11.3 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10408

[1.15.4]
* Update invoiceninja to 5.11.6
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.6)
* Fixes for template email engine by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10418
* Fixes for taxmodel migrations
* v5.11.5 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10417
* fix: issue when email body is empty by [@&#8203;paulwer](https://github.com/paulwer) in https://github.com/invoiceninja/invoiceninja/pull/10406
* Improve Storecove Proxy error handling by [@&#8203;beganovich](https://github.com/beganovich) in https://github.com/invoiceninja/invoiceninja/pull/10414
* Feature: nordigen bank_integration setup with end user agreement by [@&#8203;paulwer](https://github.com/paulwer) in https://github.com/invoiceninja/invoiceninja/pull/10410
* v5.11.4 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10415

[1.15.5]
* Update invoiceninja to 5.11.7
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.7)
* Fix for new line regression in description.
* v5.11.7 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10436

[1.15.6]
* Update invoiceninja to 5.11.9
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.9)
* Fixes for html saniftation regression of QR codes <svg> tags
* Improvements for calculate taxes
* v5.11.9 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10463
* Improve HTML sanitation
* Update translations by [@&#8203;beganovich](https://github.com/beganovich) in https://github.com/invoiceninja/invoiceninja/pull/10443

[1.15.7]
* Update invoiceninja to 5.11.10
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.10)
* v5.11.10 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10470

[1.15.8]
* Fix PDF generation

[1.15.9]
* Update invoiceninja to 5.11.11
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.11)
* Improve performance of Html purifier.
* Disable javascript in all invoice designs. (can be reversed by using DISABLE_PURIFY_HTML=true ).
* Prevent deleted payments from showing in client portal.
* v5.11.11 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10480

[1.16.0]
* Update invoiceninja to 5.11.17
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.17)
* v5.11.17 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10494
* Purify HTML: fix tagline by [@&#8203;ITSYV](https://github.com/ITSYV) in https://github.com/invoiceninja/invoiceninja/pull/10483
* v5.11.12 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10485
* Fixes for PDF's by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10486
* Reversion to ungoogled PDF generator
* Fixes for Headers not appearing in statements
* [@&#8203;ITSYV](https://github.com/ITSYV) made their first contribution in https://github.com/invoiceninja/invoiceninja/pull/10483

[1.16.1]
* Update invoiceninja to 5.11.18
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.18)
* Fixes for invoice terms display incorrectly
* Fixes for Company Imports
* Fixes for BCC not sending on payment failures
* Clean up for HTML purifier

[1.16.2]
* Update invoiceninja to 5.11.21
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.21)
* v5.11.21 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10511
* Fixes for division by zero
* v5.11.20 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10510
* Fixes for QR codes not appearing on PDF
* Revert Nordigen MR
* v5.11.19 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10506

[1.16.3]
* Update invoiceninja to 5.11.22
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.22)
* Fixes for XInvoice attachments with Purify  html class
* v5.11.22 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10518

[1.16.4]
* Update invoiceninja to 5.11.23
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.23)
* Fixes for show/hide elements by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10521

[1.16.5]
* Update invoiceninja to 5.11.24
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.24)
* Fixes for design when Enable Markdown is turned on.
* Integrate octane in preload.php by [@&#8203;benbrummer](https://github.com/benbrummer) in https://github.com/invoiceninja/invoiceninja/pull/10517
* Update invoice event when matching on invoice transactions by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10522
* v5.11.24 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10527

[1.16.6]
* Update invoiceninja to 5.11.25
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.25)
* Change filtering of scalars prior to validation
* Minor cleanup for session state
* Always update designs post self update
* Refactor for invoice deleted
* Add getters for line item discounts
* Fixes for mailable logging
* Updated dependencies
* Updated dependencies
* Updates for bulk emails
* Fixes for plan features
* Minor fixes for <br> tags in templates
* Changes for encode/decode of custom templates
* Update zimbabwe gold
* Fixes for namespace
* Expose cg settings
* Rate limiter bump
* E-invoicing: Healthchecks & regenerating tokens by [@&#8203;beganovich](https://github.com/beganovich) in https://github.com/invoiceninja/invoiceninja/pull/10525
* fix syntax error preload.php by [@&#8203;benbrummer](https://github.com/benbrummer) in https://github.com/invoiceninja/invoiceninja/pull/10531
* v5.11.25 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10536

[1.16.7]
* Update invoiceninja to 5.11.26
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.26)
* Fixes for purchase order table view in Vendor Portal
* SMTP port fixes
* Fixes for invoice query
* Update translations by [@&#8203;beganovich](https://github.com/beganovich) in https://github.com/invoiceninja/invoiceninja/pull/10538
* v5.11.26 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10539

[1.16.8]
* Update invoiceninja to 5.11.27
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.27)
* Added $swiss_qr_raw
* Check if VAT number exists on PEPPOL network prior to adding
* Custom exceptions from client portal
* Add WST Currency
* v5.11.27 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10546

[1.16.9]
* Update invoiceninja to 5.11.28
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.28)
* Fixes for user verification emails by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10548
* Nordigen custom agreements by [@&#8203;dshoreman](https://github.com/dshoreman) in https://github.com/invoiceninja/invoiceninja/pull/10526
* Prevent destructive commands in production by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10551
* Experimental new Zugferd Engine that handles inclusive/exclusive taxes along with discounts/surcharges (use the env var ZUGFERD_VERSION_TWO=true) to enable
* Prevent apple signups due to no email address provision
* Fixes for payment status when viewed from client portal
* v5.11.28 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10560

[1.16.10]
* Update invoiceninja to 5.11.29
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.29)
* Remove deprecated libxml call
* Adjustments for chrome flags
* Fix number casting prior to formatting
* Fixes for session invalidation
* Tax fixes for expenses in dashboard charts / totals
* Make GoCardless verifications an optional setting
* Enable Batch Jobs to improve Print PDF performance
* Updates for incorrect translation for small_company_info

[1.16.11]
* Update invoiceninja to 5.11.32
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.32)
* Fixes for BTC Pay webhooks
* Add + to allowed markdown
* Fixes for returning early from TaskRepository
* Fixes for sorting columns
* v5.11.32 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10592

[1.16.12]
* Update invoiceninja to 5.11.33
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.33)
* Fixes for markdown resolution
* Refactored for a single transaction when deleting an invoice.
* Update default payment method if current is deleted.
* Checks for migrations
* Updated lock file for dependencies.
* v5.11.33 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10601

[1.16.13]
* Update invoiceninja to 5.11.34
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.34)
* Fixes for recurring due dates
* Adjustments for the elegant design
* Adjustments for last login
* Fixes for recurring invoice filters
* Add checks for job_batches
* Update dependencies for Braintree
* Add no overflow when calculating due dates in recurring dates tests
* v5.11.34 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10608

[1.16.14]
* Update invoiceninja to 5.11.36
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.36)
* Revert "exclude vendor folder from chmod" by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10632
* v5.11.36 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10633
* Exclude vendor folder from chmod by [@&#8203;benbrummer](https://github.com/benbrummer) in https://github.com/invoiceninja/invoiceninja/pull/10602
* Add missing fields to FatturaPA by [@&#8203;AlessandroSechi](https://github.com/AlessandroSechi) in https://github.com/invoiceninja/invoiceninja/pull/10610
* Fixes for timing of applying credit number by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10629
* v5.11.35 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10631
* [@&#8203;AlessandroSechi](https://github.com/AlessandroSechi) made their first contribution in https://github.com/invoiceninja/invoiceninja/pull/10610

[1.17.0]
* Update invoiceninja to 5.11.37
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.37)
* Significant changes around PDF generation, removing multiple codepaths.
* Updates across all designs to improve alignment and overflow conditions.
* v5.11.37 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10636

[1.17.1]
* Update invoiceninja to 5.11.38
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.38)
* Change default Zugferd generator
* Design fixes
* Updated documentation
* Translation updates
* Prune redundant js files
* Adjustments for tax reports
* Allow table breaking in calm design
* Change email verification error code from 403 to 401
* Update for purify html allow list.
* Fixes for tests by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10637
* v5.11.38 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10658

[1.17.2]
* Update invoiceninja to 5.11.39
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.39)
* Ensure primary contact is preferences in reports
* Quickbooks imports
* Update INR symbol

[1.17.3]
* Update invoiceninja to 5.11.40
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.40)
* Fixes for invoicing projects. by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10664

[1.17.4]
* Update invoiceninja to 5.11.41
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.41)
* Change PDF Mock dates to ones that are parseable by twig
* Test einvoice properties at the invoice level
* API implementation of InvoicePeriod for Invoices and Recurring Invoices
* Added e_invoice column to recurring invoices (Invoice Period Support)
* Implement options for the display of HTML invoices in the client portal
* Fixes for encoded URLs in twig templates
* QB Backup imports
* Handle chunked file uploads by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10674
* v5.11.41 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10686

[1.17.5]
* Update invoiceninja to 5.11.42
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.42)

[1.18.0]
* Update base image to 5

[1.18.1]
* Update invoiceninja to 5.11.43
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.43)
* Minor fixes for test scenario by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10715
* v5.11.43 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10717

[1.18.2]
* Update invoiceninja to 5.11.48
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.48)
* Fixes for company imports
* v5.11.48 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10724
* Fixes for Zugferd Tax Business Categorization
* v5.11.46 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10723
* Fixes for zugferd document
* v5.11.45 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10722

[1.18.3]
* Update invoiceninja to 5.11.49
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.49)
* Zugferd fixes
* Prevent Client and Contact names from duplicating on PDFs
* New React UI
* v5.11.49 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10727

[1.18.4]
* Update invoiceninja to 5.11.51
* [Full Changelog](https://github.com/invoiceninja/invoiceninja/releases/tag/v5.11.51)
* exclude vendor folder second attempt by [@&#8203;benbrummer](https://github.com/benbrummer) in https://github.com/invoiceninja/invoiceninja/pull/10642
* v5.11.51 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10733
* Fixes for ZugferdDocument creation for VAT not registered businesses
* v5.11.50 by [@&#8203;turbo124](https://github.com/turbo124) in https://github.com/invoiceninja/invoiceninja/pull/10731

